use anyhow::{bail, Context, Error};
use std::cmp::{max, min};
use std::collections::HashMap;

#[derive(Debug)]
enum Instruction {
    Left,
    Right,
}

impl TryFrom<char> for Instruction {
    type Error = Error;

    fn try_from(value: char) -> std::result::Result<Self, Self::Error> {
        let instruction = match value {
            'L' => Self::Left,
            'R' => Self::Right,
            _ => bail!("Wrong instruction"),
        };

        Ok(instruction)
    }
}

#[derive(Debug)]
struct Node {
    id: String,
    left: String,
    right: String,
}

impl Node {
    fn get_next(&self, instruction: &Instruction) -> String {
        match instruction {
            Instruction::Left => self.left.clone(),
            Instruction::Right => self.right.clone(),
        }
    }
}

impl TryFrom<&String> for Node {
    type Error = Error;

    fn try_from(value: &String) -> std::result::Result<Self, Self::Error> {
        if value.len() != 16 {
            bail!("Wrong input for Node")
        }

        let id = value[0..3].to_string();
        let left = value[7..10].to_string();
        let right = value[12..15].to_string();

        Ok(Self { id, left, right })
    }
}

#[derive(Debug)]
pub struct Map {
    instructions: Vec<Instruction>,
    nodes: HashMap<String, Node>,
}

impl Map {
    pub fn parse(lines: Vec<String>) -> anyhow::Result<Self> {
        if lines.len() < 3 {
            bail!("Wrong input");
        }

        let instructions = lines[0]
            .chars()
            .map(|instruction| instruction.try_into())
            .collect::<anyhow::Result<Vec<_>>>()?;

        let nodes = lines
            .iter()
            .skip(2)
            .map(|node| {
                let node: Node = node.try_into()?;

                Ok((node.id.clone(), node))
            })
            .collect::<anyhow::Result<HashMap<_, _>>>()?;

        Ok(Self {
            instructions,
            nodes,
        })
    }

    pub fn count_steps(&self) -> anyhow::Result<u64> {
        let mut from = "AAA".to_string();

        let to = "ZZZ".to_string();

        let mut steps = 0;

        'repeat: loop {
            for instruction in &self.instructions {
                let node = self.nodes.get(from.as_str()).context("Node not found")?;

                from = node.get_next(instruction);

                steps += 1;

                if from == to {
                    break 'repeat;
                }
            }
        }

        Ok(steps)
    }

    fn steps_to_end(&self, mut from: String) -> anyhow::Result<u64> {
        let mut steps = 0;

        'repeat: loop {
            for instruction in &self.instructions {
                let node = self.nodes.get(from.as_str()).context("Node not found")?;

                from = node.get_next(instruction);

                steps += 1;

                if from.ends_with("Z") {
                    break 'repeat;
                }
            }
        }

        Ok(steps)
    }

    fn gcd(first: u64, second: u64) -> u64 {
        let mut min = min(first, second);
        let mut max = max(first, second);

        loop {
            let r = max % min;

            if r == 0 {
                return min;
            }

            max = min;
            min = r;
        }
    }

    fn lcm(first: u64, second: u64) -> u64 {
        first * second / Self::gcd(first, second)
    }

    pub fn count_steps2(&self) -> anyhow::Result<u64> {
        let mut steps = 1;

        let from_nodes: Vec<String> = self
            .nodes
            .iter()
            .map(|(id, _)| id.clone())
            .filter(|node| node.ends_with("A"))
            .collect();

        for node in from_nodes {
            let steps_to_end = self.steps_to_end(node.clone())?;

            steps = Self::lcm(steps_to_end, steps);
        }

        Ok(steps)
    }
}
