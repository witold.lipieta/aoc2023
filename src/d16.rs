use anyhow::bail;
use std::collections::HashSet;

type Coordinates = (usize, usize);

#[derive(Debug, Copy, Clone, Eq, Hash, PartialEq)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn shift(&self, coords: Coordinates, rows: usize, cols: usize) -> Option<Coordinates> {
        match self {
            Direction::North => {
                if coords.0 > 0 {
                    Some((coords.0 - 1, coords.1))
                } else {
                    None
                }
            }
            Direction::East => {
                if coords.1 < cols - 1 {
                    Some((coords.0, coords.1 + 1))
                } else {
                    None
                }
            }
            Direction::South => {
                if coords.0 < rows - 1 {
                    Some((coords.0 + 1, coords.1))
                } else {
                    None
                }
            }
            Direction::West => {
                if coords.1 > 0 {
                    Some((coords.0, coords.1 - 1))
                } else {
                    None
                }
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
enum Tile {
    Empty,
    MirrorSlash,
    MirrorBackslash,
    SplitterVertical,
    SplitterHorizontal,
}

impl Tile {
    fn parse(symbol: char) -> anyhow::Result<Self> {
        let tile = match symbol {
            '.' => Self::Empty,
            '/' => Self::MirrorSlash,
            '\\' => Self::MirrorBackslash,
            '|' => Self::SplitterVertical,
            '-' => Self::SplitterHorizontal,
            _ => bail!("Wrong input"),
        };

        Ok(tile)
    }
}

#[derive(Debug, Clone)]
pub struct Energizer {
    rows: usize,
    cols: usize,
    tiles: Vec<Vec<Tile>>,
    energy: Vec<Vec<bool>>,
    visited: Vec<Vec<HashSet<Direction>>>,
}

impl Energizer {
    pub fn score(&self) -> usize {
        self.energy
            .iter()
            .map(|r| r.iter().filter(|e| **e).count())
            .sum()
    }
    pub fn score2(&mut self) -> Option<usize> {
        let max_top = (0..self.cols)
            .into_iter()
            .map(|col| {
                let mut e = self.clone();
                e.cast_beam((0, col), Direction::South);

                e.score()
            })
            .max();

        let max_bottom = (0..self.cols)
            .into_iter()
            .map(|col| {
                let mut e = self.clone();
                e.cast_beam((self.rows - 1, col), Direction::North);

                e.score()
            })
            .max();

        let max_left = (0..self.rows)
            .into_iter()
            .map(|row| {
                let mut e = self.clone();
                e.cast_beam((row, 0), Direction::East);

                e.score()
            })
            .max();

        let max_right = (0..self.rows)
            .into_iter()
            .map(|row| {
                let mut e = self.clone();
                e.cast_beam((row, self.cols - 1), Direction::West);

                e.score()
            })
            .max();

        let max = [max_top, max_bottom, max_left, max_right]
            .into_iter()
            .flatten()
            .max();

        max
    }

    fn next(&self, direction: Direction, from: Coordinates) -> Option<Coordinates> {
        direction.shift(from, self.rows, self.cols)
    }

    pub fn cast_beam(&mut self, from: Coordinates, direction: Direction) {
        if self.visited[from.0][from.1].contains(&direction) {
            return;
        }
        self.visited[from.0][from.1].insert(direction);
        self.energy[from.0][from.1] = true;

        let (next_direction_a, next_direction_b) = match (&self.tiles[from.0][from.1], direction) {
            (Tile::Empty, dir) => (Some(dir), None),
            (Tile::MirrorBackslash, Direction::East) => (Some(Direction::South), None),
            (Tile::MirrorBackslash, Direction::North) => (Some(Direction::West), None),
            (Tile::MirrorBackslash, Direction::West) => (Some(Direction::North), None),
            (Tile::MirrorBackslash, Direction::South) => (Some(Direction::East), None),
            (Tile::MirrorSlash, Direction::East) => (Some(Direction::North), None),
            (Tile::MirrorSlash, Direction::North) => (Some(Direction::East), None),
            (Tile::MirrorSlash, Direction::West) => (Some(Direction::South), None),
            (Tile::MirrorSlash, Direction::South) => (Some(Direction::West), None),
            (Tile::SplitterVertical, Direction::East) => {
                (Some(Direction::North), Some(Direction::South))
            }
            (Tile::SplitterVertical, Direction::North) => (Some(Direction::North), None),
            (Tile::SplitterVertical, Direction::West) => {
                (Some(Direction::North), Some(Direction::South))
            }
            (Tile::SplitterVertical, Direction::South) => (Some(Direction::South), None),
            (Tile::SplitterHorizontal, Direction::East) => (Some(Direction::East), None),
            (Tile::SplitterHorizontal, Direction::North) => {
                (Some(Direction::East), Some(Direction::West))
            }
            (Tile::SplitterHorizontal, Direction::West) => (Some(Direction::West), None),
            (Tile::SplitterHorizontal, Direction::South) => {
                (Some(Direction::East), Some(Direction::West))
            }
        };

        if let Some(next_direction) = next_direction_a {
            if let Some(next_coords) = self.next(next_direction, from) {
                self.cast_beam(next_coords, next_direction);
            }
        }

        if let Some(next_direction) = next_direction_b {
            if let Some(next_coords) = self.next(next_direction, from) {
                self.cast_beam(next_coords, next_direction);
            }
        }
    }

    pub fn parse(lines: Vec<String>) -> anyhow::Result<Self> {
        if lines.is_empty() {
            bail!("Empty input data")
        }

        let rows = lines.len();
        let cols = lines[0].len();

        let energy = vec![vec![false; cols]; rows];

        let visited = vec![vec![HashSet::new(); cols]; rows];

        let mut tiles = vec![Vec::new(); rows];

        for (row_index, line) in lines.into_iter().enumerate() {
            for symbol in line.chars() {
                let tile = Tile::parse(symbol)?;

                tiles[row_index].push(tile);
            }
        }

        Ok(Self {
            rows,
            cols,
            tiles,
            energy,
            visited,
        })
    }
}
