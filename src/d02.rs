use anyhow::{bail, Context, Error};
use std::cmp::max;

struct CubesSet {
    red: i32,
    green: i32,
    blue: i32,
}

impl CubesSet {
    fn parse_entry(&mut self, entry: &str) -> anyhow::Result<()> {
        if let Some(count) = entry.strip_suffix(" red") {
            self.red = count.parse().context("Failed to parse red count")?;
        } else if let Some(count) = entry.strip_suffix(" green") {
            self.green = count.parse().context("Failed to parse green count")?;
        } else if let Some(count) = entry.strip_suffix(" blue") {
            self.blue = count.parse().context("Failed to parse blue count")?;
        } else {
            bail!("Wrong format of input data")
        }

        Ok(())
    }

    fn is_possible(&self) -> bool {
        self.red <= 12 && self.green <= 13 && self.blue <= 14
    }

    fn expand(&mut self, other: &Self) {
        self.red = max(self.red, other.red);
        self.green = max(self.green, other.green);
        self.blue = max(self.blue, other.blue);
    }
}

impl TryFrom<&str> for CubesSet {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut cubes_set = Self {
            red: 0,
            green: 0,
            blue: 0,
        };

        for entry in value.split(",") {
            cubes_set.parse_entry(entry.trim())?;
        }

        Ok(cubes_set)
    }
}

pub struct Game {
    id: i32,
    results: Vec<CubesSet>,
}

impl Game {
    fn get_id(header: &str) -> anyhow::Result<i32> {
        if let Some(id) = header.strip_prefix("Game ") {
            id.parse().context("Failed to parse ID")
        } else {
            bail!("Wrong format of input data")
        }
    }

    fn is_possible(&self) -> bool {
        for result in &self.results {
            if !result.is_possible() {
                return false;
            }
        }

        true
    }

    pub fn score(&self) -> i32 {
        if self.is_possible() {
            self.id
        } else {
            0
        }
    }

    fn minimum_set(&self) -> CubesSet {
        let mut minimum_set = CubesSet {
            red: 0,
            green: 0,
            blue: 0,
        };

        for result in &self.results {
            minimum_set.expand(result)
        }

        minimum_set
    }

    pub fn score2(&self) -> i32 {
        let minimum_set = self.minimum_set();

        minimum_set.red * minimum_set.green * minimum_set.blue
    }
}

impl TryFrom<&str> for Game {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let record = value.split(":").collect::<Vec<_>>();

        if record.len() != 2 {
            bail!("Wrong format of input data")
        }

        let id = Game::get_id(record[0])?;

        let results = record[1]
            .split(";")
            .into_iter()
            .map(|result| result.try_into())
            .collect::<anyhow::Result<Vec<_>>>()?;

        Ok(Self { id, results })
    }
}
