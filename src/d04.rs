use anyhow::{bail, Context, Error};
use std::cmp::min;
use std::collections::HashSet;

#[derive(Debug)]
pub struct ScratchCard {
    winning: HashSet<u32>,
    drawn: HashSet<u32>,
}

impl ScratchCard {
    fn winners_count(&self) -> u32 {
        self.winning.intersection(&self.drawn).count() as u32
    }
}

impl TryFrom<&str> for ScratchCard {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let card = value.split(":").collect::<Vec<_>>();

        if card.len() != 2 {
            bail!("Wrong format of input data")
        }

        let numbers = card[1].split("|").collect::<Vec<_>>();

        if numbers.len() != 2 {
            bail!("Wrong format of input data")
        }

        let winning = numbers[0]
            .split_whitespace()
            .map(|number| number.parse().context("Failed to parse winning number"))
            .collect::<anyhow::Result<HashSet<_>>>()?;

        let drawn = numbers[1]
            .split_whitespace()
            .map(|number| number.parse().context("Failed to parse drawn number"))
            .collect::<anyhow::Result<HashSet<_>>>()?;

        Ok(Self { winning, drawn })
    }
}

#[derive(Debug)]
pub struct Lottery {
    cards_winners_counts: Vec<u32>,
}

impl Lottery {
    pub fn score(&self) -> u32 {
        self.cards_winners_counts
            .iter()
            .map(|card_winners| {
                if *card_winners > 0 {
                    2u32.pow(*card_winners - 1)
                } else {
                    0
                }
            })
            .sum::<u32>()
    }

    pub fn score2(&self) -> u32 {
        let mut cards_counts = vec![1; self.cards_winners_counts.len()];

        for (index, card_winners) in self.cards_winners_counts.iter().enumerate() {
            let cards_left = self.cards_winners_counts.len() - 1 - index;

            let cards_left_to_win = min(cards_left, *card_winners as usize);

            for copy_won_index in index + 1..index + 1 + cards_left_to_win {
                cards_counts[copy_won_index] += cards_counts[index];
            }
        }

        cards_counts.iter().sum()
    }
}

impl TryFrom<Vec<String>> for Lottery {
    type Error = Error;

    fn try_from(lines: Vec<String>) -> Result<Self, Self::Error> {
        let cards = lines
            .iter()
            .map(|line| TryInto::<ScratchCard>::try_into(line.as_str()))
            .collect::<anyhow::Result<Vec<_>>>()
            .context("Failed to parse input data")?;

        let cards = cards
            .iter()
            .map(|card| card.winners_count())
            .collect::<Vec<_>>();

        Ok(Self {
            cards_winners_counts: cards,
        })
    }
}
