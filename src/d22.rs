use anyhow::{bail, Context};
use std::collections::{BTreeMap, BTreeSet, HashSet};

#[derive(Debug, Clone)]
struct Brick {
    w: usize,
    l: usize,
    h: usize,
    x: usize,
    y: usize,
    z: usize,
}

impl Brick {
    fn is_supported_by(&self, other: &Self) -> bool {
        let top_z = other.z + other.h - 1;

        if top_z != self.z - 1 {
            return false;
        }

        for x_cube in self.x..self.x + self.w {
            for y_cube in self.y..self.y + self.l {
                if (other.x..other.x + other.w).contains(&x_cube)
                    && (other.y..other.y + other.l).contains(&y_cube)
                {
                    return true;
                }
            }
        }

        false
    }

    fn parse(input: String) -> anyhow::Result<Self> {
        let (s, e) = input.split_once("~").context("Wrong brick coords")?;

        let s = s
            .split(",")
            .map(|c| c.parse().context("Failed to parse coord"))
            .collect::<anyhow::Result<Vec<usize>>>()?;
        let e = e
            .split(",")
            .map(|c| c.parse().context("Failed to parse coord"))
            .collect::<anyhow::Result<Vec<usize>>>()?;

        if s.len() != 3 || e.len() != 3 {
            bail!("Wrong input")
        }

        let xs = s[0];
        let ys = s[1];
        let zs = s[2];
        let xe = e[0];
        let ye = e[1];
        let ze = e[2];

        Ok(Self {
            w: xe.abs_diff(xs) + 1,
            l: ye.abs_diff(ys) + 1,
            h: ze.abs_diff(zs) + 1,
            x: xs,
            y: ys,
            z: zs,
        })
    }
}

#[derive(Debug)]
pub struct Snapshot {
    bricks: Vec<Brick>,
}

impl Snapshot {
    pub fn collapsed_count(&mut self) -> usize {
        let supported_by = self.fall();

        let mut supporting_bricks = BTreeSet::new();

        for (_, supports) in &supported_by {
            if supports.len() == 1 {
                for elem in supports {
                    supporting_bricks.insert(elem);
                }
            }
        }

        let original_len = supported_by.len();
        let mut count = 0;

        for supporting_brick in supporting_bricks {
            let mut supported_by = supported_by.clone();
            Self::collapse(&mut supported_by, *supporting_brick);

            let collapsed_count = original_len - supported_by.len();

            count += collapsed_count;
        }

        count
    }

    fn collapse(supported_by: &mut BTreeMap<usize, BTreeSet<usize>>, removed_brick: usize) {
        let mut collapsed_bricks = HashSet::new();
        for (collapsed, sby) in supported_by.iter_mut() {
            sby.remove(&removed_brick);

            if sby.is_empty() {
                collapsed_bricks.insert(*collapsed);
            }
        }

        for collapsed_brick in &collapsed_bricks {
            supported_by.remove(collapsed_brick);
        }

        for collapsed_brick in collapsed_bricks {
            Self::collapse(supported_by, collapsed_brick);
        }
    }

    pub fn can_be_removed(&mut self) -> usize {
        let supported_by = self.fall();

        let mut can_be_removed = (0..self.bricks.len()).collect::<BTreeSet<usize>>();

        for (_, supports) in &supported_by {
            if supports.len() == 1 {
                for elem in supports {
                    can_be_removed.remove(elem);
                }
            }
        }

        can_be_removed.iter().count()
    }

    fn fall(&mut self) -> BTreeMap<usize, BTreeSet<usize>> {
        let mut supported_by: BTreeMap<usize, BTreeSet<usize>> = BTreeMap::new();

        for brick_i in 0..self.bricks.len() {
            let mut is_supported = false;

            for new_z in (1..self.bricks[brick_i].z + 1).rev() {
                self.bricks[brick_i].z = new_z;

                if self.bricks[brick_i].z == 1 {
                    // resting on ground
                    break;
                }

                for previous_brick_i in 0..brick_i {
                    if self.bricks[brick_i].is_supported_by(&self.bricks[previous_brick_i]) {
                        // cannot move any lower
                        is_supported = true;
                        supported_by
                            .entry(brick_i)
                            .and_modify(|v| {
                                v.insert(previous_brick_i);
                            })
                            .or_insert(BTreeSet::from([previous_brick_i]));
                    }
                }

                if is_supported {
                    // resting on another brick(s)
                    break;
                }
            }
        }

        supported_by
    }

    pub fn parse(input: Vec<String>) -> anyhow::Result<Self> {
        let mut bricks = input
            .into_iter()
            .map(|l| Brick::parse(l))
            .collect::<anyhow::Result<Vec<_>>>()?;

        bricks.sort_by_key(|b| b.z);

        Ok(Self { bricks })
    }
}
