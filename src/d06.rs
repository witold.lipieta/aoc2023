use anyhow::{bail, Context};
use std::iter::zip;

#[derive(Debug)]
struct Record {
    time: u64,
    distance: u64,
}

impl Record {
    pub fn new(time: u64, distance: u64) -> Self {
        Self { time, distance }
    }

    fn distance_for_charge(&self, charge: u64) -> u64 {
        if charge > self.time {
            return 0;
        }

        charge * (self.time - charge)
    }

    fn count_possible_winners(&self) -> usize {
        (1..self.time)
            .into_iter()
            .filter(|charge| self.distance_for_charge(*charge) > self.distance)
            .count()
    }
}

#[derive(Debug)]
pub struct Races {
    records: Vec<Record>,
}

impl Races {
    pub fn parse(lines: Vec<String>) -> anyhow::Result<Self> {
        if lines.len() != 2 {
            bail!("Wrong input");
        }

        let times = if let Some(times) = lines[0].strip_prefix("Time:") {
            times
                .split_whitespace()
                .map(|time| time.parse().context("Couldn't parse time"))
                .collect::<anyhow::Result<Vec<_>>>()?
        } else {
            bail!("Wrong input");
        };

        let distances = if let Some(distances) = lines[1].strip_prefix("Distance:") {
            distances
                .split_whitespace()
                .map(|distance| distance.parse().context("Couldn't parse distance"))
                .collect::<anyhow::Result<Vec<_>>>()?
        } else {
            bail!("Wrong input");
        };

        if times.len() != distances.len() {
            bail!("Wrong input");
        }

        let records = zip(times, distances)
            .map(|(time, distance)| Record::new(time, distance))
            .collect();

        Ok(Self { records })
    }

    pub fn parse2(lines: Vec<String>) -> anyhow::Result<Self> {
        if lines.len() != 2 {
            bail!("Wrong input");
        }

        let time = if let Some(times) = lines[0].strip_prefix("Time:") {
            times
                .split_whitespace()
                .collect::<String>()
                .parse()
                .context("Couldn't parse time")?
        } else {
            bail!("Wrong input");
        };

        let distance = if let Some(distances) = lines[1].strip_prefix("Distance:") {
            distances
                .split_whitespace()
                .collect::<String>()
                .parse()
                .context("Couldn't parse distance")?
        } else {
            bail!("Wrong input");
        };

        let mut records = Vec::new();

        records.push(Record::new(time, distance));

        Ok(Self { records })
    }

    pub fn score(&self) -> usize {
        let mut score = 1;

        for race in &self.records {
            score *= race.count_possible_winners();
        }

        score
    }
}
