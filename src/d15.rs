use anyhow::Context;
use std::collections::HashMap;

#[derive(Debug)]
enum Operation {
    None,
    Dash,
    Equals(usize),
}

#[derive(Debug)]
struct InitializationStep {
    label: String,
    operation: Operation,
}

impl InitializationStep {
    fn hash(&self) -> usize {
        let mut current_value = 0;

        for character in self.label.chars() {
            let ascii_code = character as usize;
            current_value += ascii_code;
            current_value *= 17;
            current_value %= 256;
        }

        current_value
    }

    fn parse2(value: &str) -> anyhow::Result<Self> {
        if let Some(label) = value.strip_suffix("-") {
            Ok(Self {
                label: label.to_string(),
                operation: Operation::Dash,
            })
        } else {
            let mut step_string = value.chars();

            step_string.next_back();
            step_string.next_back();

            let step_string = step_string.as_str().to_string();

            let focal_length = value
                .chars()
                .last()
                .unwrap()
                .to_digit(10)
                .context("Wrong input data format")?;

            Ok(Self {
                label: step_string,
                operation: Operation::Equals(focal_length as usize),
            })
        }
    }
}

impl From<&str> for InitializationStep {
    fn from(value: &str) -> Self {
        let step_string = value.to_owned();

        Self {
            label: step_string,
            operation: Operation::None,
        }
    }
}

#[derive(Debug)]
pub struct InitializationSequence {
    boxes: Vec<HashMap<String, usize>>,
    order: Vec<HashMap<String, usize>>,
    next_order: Vec<usize>,
    steps: Vec<InitializationStep>,
}

impl InitializationSequence {
    pub fn score(&self) -> usize {
        self.steps.iter().map(|step| step.hash()).sum()
    }

    fn sequence(&mut self) {
        for step in &self.steps {
            match step.operation {
                Operation::None => {}
                Operation::Dash => {
                    let hash = step.hash();

                    self.order[hash].remove(&step.label);
                    self.boxes[hash].remove(&step.label);
                }
                Operation::Equals(focal_length) => {
                    let hash = step.hash();

                    if let None = self.boxes[hash].insert(step.label.clone(), focal_length) {
                        self.order[hash].insert(step.label.clone(), self.next_order[hash]);
                        self.next_order[hash] += 1;
                    }
                }
            }
        }
    }

    pub fn score2(&mut self) -> usize {
        self.sequence();

        let mut sum = 0;

        for box_i in 0..256 {
            let mut order_list = self.order[box_i].iter().collect::<Vec<_>>();

            order_list.sort_by_key(|(_, order)| **order);

            for (index, (label, _)) in order_list.into_iter().enumerate() {
                let focal_length = self.boxes[box_i].get(label).unwrap();

                sum += (box_i + 1) * (index + 1) * *focal_length;
            }
        }

        sum
    }

    pub fn parse(input: String) -> anyhow::Result<Self> {
        let boxes = Vec::new();
        let order = Vec::new();
        let next_order = Vec::new();
        let steps = input.split(",").map(|step| step.into()).collect();

        Ok(Self {
            boxes,
            order,
            next_order,
            steps,
        })
    }

    pub fn parse2(input: String) -> anyhow::Result<Self> {
        let boxes = vec![HashMap::new(); 256];
        let order = vec![HashMap::new(); 256];
        let next_order = vec![0; 256];
        let steps = input
            .split(",")
            .map(|step| InitializationStep::parse2(step))
            .collect::<anyhow::Result<_>>()?;

        Ok(Self {
            boxes,
            order,
            next_order,
            steps,
        })
    }
}
