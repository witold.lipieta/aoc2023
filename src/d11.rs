use anyhow::bail;

#[derive(Debug)]
struct Galaxy {
    row: usize,
    col: usize,
}

impl Galaxy {
    pub fn new(row: usize, col: usize) -> Self {
        Self { row, col }
    }

    fn distance(&self, other: &Self) -> usize {
        self.row.abs_diff(other.row) + self.col.abs_diff(other.col)
    }
}

#[derive(Debug)]
pub struct Universe {
    // image: Vec<Vec<bool>>,
    galaxies: Vec<Galaxy>,
}

impl Universe {
    pub fn sum_of_distances(&self) -> usize {
        self.galaxies
            .iter()
            .enumerate()
            .flat_map(|(index, galaxy)| {
                self.galaxies[index + 1..]
                    .iter()
                    .map(|other_galaxy| galaxy.distance(other_galaxy))
            })
            .sum::<usize>()
    }

    pub fn parse(lines: Vec<String>, expand_by: usize) -> anyhow::Result<Self> {
        if lines.is_empty() {
            bail!("Empty input data")
        }

        let mut galaxies = Vec::new();

        let mut rows_to_expand = vec![true; lines.len()];

        let mut cols_to_expand = vec![true; lines[0].len()];

        for (row_index, line) in lines.into_iter().enumerate() {
            for (col_index, symbol) in line.chars().enumerate() {
                if symbol == '#' {
                    galaxies.push(Galaxy::new(row_index, col_index));

                    rows_to_expand[row_index] = false;

                    cols_to_expand[col_index] = false;
                }
            }
        }

        // println!("rows to expand {:?}", rows_to_expand);
        // println!("cols to expand {:?}", cols_to_expand);

        for (row_index, expand) in rows_to_expand.iter().enumerate().rev() {
            if *expand {
                for galaxy in galaxies.iter_mut().filter(|galaxy| galaxy.row > row_index) {
                    galaxy.row += expand_by;
                }
            }
        }

        for (col_index, expand) in cols_to_expand.iter().enumerate().rev() {
            if *expand {
                for galaxy in galaxies.iter_mut().filter(|galaxy| galaxy.col > col_index) {
                    galaxy.col += expand_by;
                }
            }
        }

        Ok(Universe { galaxies })
    }
}
