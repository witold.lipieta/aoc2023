use anyhow::{bail, Context, Error};
use std::cmp::{max, min};
use std::ops::{Add, AddAssign, Range};

#[derive(Debug, Clone)]
struct ExtendedRange<T> {
    range: Range<T>,
}

impl<T> ExtendedRange<T>
where
    T: Ord + Copy + Add<Output = T>,
{
    fn union(&self, other: &Self) -> Self {
        let range = max(self.range.start, other.range.start)..min(self.range.end, other.range.end);

        Self { range }
    }

    fn prefix(&self, other: &Self) -> Self {
        let range = other.range.start..min(other.range.end, self.range.start);

        Self { range }
    }

    fn suffix(&self, other: &Self) -> Self {
        let range = max(self.range.end, other.range.start)..other.range.end;

        Self { range }
    }

    fn new(start: T, length: T) -> Self {
        Self {
            range: (start..start + length),
        }
    }

    fn is_empty(&self) -> bool {
        self.range.is_empty()
    }

    fn min(&self) -> Option<T> {
        if self.is_empty() {
            None
        } else {
            Some(self.range.start)
        }
    }

    fn shifted(&self, offset: T) -> Self {
        Self {
            range: (self.range.start + offset..self.range.end + offset),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Seeds {
    ranges: Vec<ExtendedRange<i64>>,
}

impl Seeds {
    pub fn min(&self) -> Option<i64> {
        self.ranges.iter().map(|range| range.min()).min().flatten()
    }

    fn new() -> Self {
        Self { ranges: Vec::new() }
    }

    fn append(&mut self, item: ExtendedRange<i64>) {
        self.ranges.push(item);
    }

    fn parse(line: &str) -> anyhow::Result<Self> {
        let seeds = Self::parse_line(line)?;

        Ok(Self {
            ranges: seeds
                .iter()
                .map(|seed| ExtendedRange::new(*seed, 1))
                .collect(),
        })
    }

    fn parse2(line: &str) -> anyhow::Result<Self> {
        let seeds = Self::parse_line(line)?;

        let mut ranges = Vec::new();

        if seeds.len() % 2 != 0 {
            bail!("Failed to parse seeds ranges")
        }

        for range_index in 0..seeds.len() / 2 {
            ranges.push(ExtendedRange::new(
                seeds[range_index * 2],
                seeds[range_index * 2 + 1],
            ))
        }

        Ok(Self { ranges })
    }

    fn parse_line(line: &str) -> Result<Vec<i64>, Error> {
        let seeds = if let Some(seeds) = line.strip_prefix("seeds: ") {
            seeds
                .split_whitespace()
                .map(|seed| seed.parse().context("Couldn't parse seed number"))
                .collect::<anyhow::Result<Vec<_>>>()?
        } else {
            bail!("Wrong format of input data");
        };
        Ok(seeds)
    }
}

impl Add for Seeds {
    type Output = Seeds;

    fn add(self, rhs: Self) -> Self::Output {
        let mut out = Self::new();

        out.ranges = self.ranges.clone();

        out.ranges.extend(rhs.ranges);

        out
    }
}

impl AddAssign for Seeds {
    fn add_assign(&mut self, rhs: Self) {
        self.ranges.extend(rhs.ranges)
    }
}

#[derive(Debug)]
struct Conversion {
    range: ExtendedRange<i64>,
    offset: i64,
}

impl Conversion {
    fn convert_range(
        &self,
        other: &ExtendedRange<i64>,
    ) -> (ExtendedRange<i64>, ExtendedRange<i64>, ExtendedRange<i64>) {
        let union = self.range.union(other);

        let converted = union.shifted(self.offset);

        (
            converted,
            self.range.prefix(&other),
            self.range.suffix(&other),
        )
    }

    fn convert_seeds(&self, seeds: &Seeds) -> (Seeds, Seeds) {
        let mut converted_seeds = Seeds::new();
        let mut remaining_seeds = Seeds::new();

        for seeds_range in &seeds.ranges {
            let (converted, prefix, suffix) = self.convert_range(seeds_range);

            if !converted.is_empty() {
                converted_seeds.append(converted);
            }

            if !prefix.is_empty() {
                remaining_seeds.append(prefix);
            }

            if !suffix.is_empty() {
                remaining_seeds.append(suffix);
            }
        }

        (converted_seeds, remaining_seeds)
    }
}

impl TryFrom<&str> for Conversion {
    type Error = Error;

    fn try_from(line: &str) -> std::result::Result<Self, Self::Error> {
        let conversion = line
            .split_whitespace()
            .map(|n| n.parse().context("Failed to parse conversion number"))
            .collect::<anyhow::Result<Vec<i64>>>()?;

        if conversion.len() != 3 {
            bail!("Couldn't parse conversion line")
        }

        Ok(Self {
            range: ExtendedRange::new(conversion[1], conversion[2]),
            offset: conversion[0] - conversion[1],
        })
    }
}

#[derive(Debug)]
struct ConversionMap {
    conversions: Vec<Conversion>,
}

impl ConversionMap {
    fn convert(&self, seeds: Seeds) -> Seeds {
        let mut remaining_seeds = seeds;
        let mut converted_seeds = Seeds::new();

        for conversion in &self.conversions {
            let (converted, remaining) = conversion.convert_seeds(&remaining_seeds);

            remaining_seeds = remaining;
            converted_seeds += converted;
        }

        converted_seeds + remaining_seeds
    }
}

impl TryFrom<&str> for ConversionMap {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        if !value
            .split("\n")
            .nth(0)
            .context("Not enough data to parse conversion map")?
            .ends_with("map:")
        {
            bail!("Wrong format of input data")
        }

        let conversions = value
            .split("\n")
            .skip(1)
            .map(|line| line.try_into())
            .collect::<anyhow::Result<Vec<Conversion>>>()?;

        Ok(Self { conversions })
    }
}

#[derive(Debug)]
pub struct Almanac {
    seeds: Seeds,
    conversion_maps: Vec<ConversionMap>,
}

impl Almanac {
    pub fn get_seeds_locations(&self) -> Seeds {
        let mut seeds = self.seeds.clone();

        for conversion_map in &self.conversion_maps {
            seeds = conversion_map.convert(seeds);
        }

        seeds
    }

    pub fn parse(lines: Vec<String>) -> anyhow::Result<Self> {
        if lines.len() < 3 {
            bail!("Not enough input")
        }

        let seeds = Seeds::parse(lines[0].as_str())?;

        let conversion_maps = Self::conversion_maps(lines)?;

        Ok(Self {
            seeds,
            conversion_maps,
        })
    }

    pub fn parse2(lines: Vec<String>) -> anyhow::Result<Self> {
        if lines.len() < 3 {
            bail!("Not enough input")
        }

        let seeds = Seeds::parse2(lines[0].as_str())?;

        let conversion_maps = Self::conversion_maps(lines)?;

        Ok(Self {
            seeds,
            conversion_maps,
        })
    }

    fn conversion_maps(lines: Vec<String>) -> anyhow::Result<Vec<ConversionMap>, Error> {
        // input shouldn't be split to lines...
        let conversion_maps = lines[2..]
            .join("\n")
            .split("\n\n")
            .map(|conversion_map| conversion_map.try_into())
            .collect::<anyhow::Result<_>>()?;

        Ok(conversion_maps)
    }
}
