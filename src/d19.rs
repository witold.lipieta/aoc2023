use anyhow::{bail, Context};
use std::collections::{HashMap, VecDeque};
use std::ops::Range;

#[derive(Debug)]
enum Condition {
    GreaterThan,
    LessThan,
}

#[derive(Debug)]
enum Category {
    ExtremelyCoolLooking,
    Musical,
    Aerodynamic,
    Shiny,
}

#[derive(Debug)]
struct Part {
    x: usize,
    m: usize,
    a: usize,
    s: usize,
}

impl Part {
    fn parse(value: &str) -> anyhow::Result<Self> {
        let value = value
            .strip_prefix("{")
            .context("Wrong part prefix input data")?;
        let value = value
            .strip_suffix("}")
            .context("Wrong part suffix input data")?;

        let categories = value.split(",").collect::<Vec<_>>();

        if categories.len() != 4 {
            bail!("Wrong part input data format");
        }

        let x = Self::get_category_value(categories[0], "x")?;
        let m = Self::get_category_value(categories[1], "m")?;
        let a = Self::get_category_value(categories[2], "a")?;
        let s = Self::get_category_value(categories[3], "s")?;

        Ok(Self { x, m, a, s })
    }

    fn get_category_value(category_data: &str, category_name: &str) -> anyhow::Result<usize> {
        let (category, value) = category_data
            .split_once("=")
            .context("Wrong category part input data")?;

        if category != category_name {
            bail!("Wrong part category name");
        }

        value.parse().context("Wrong category part value")
    }
}

#[derive(Debug, Clone)]
struct PartsRange {
    x: Range<usize>,
    m: Range<usize>,
    a: Range<usize>,
    s: Range<usize>,
}

#[derive(Debug, Clone)]
enum Target {
    Accept,
    Reject,
    Workflow(String),
}

#[derive(Debug)]
struct RuleInfo {
    category: Category,
    condition: Condition,
    threshold: usize,
    target: Target,
}

#[derive(Debug)]
enum Rule {
    BasicRule(Target),
    ConditionalRule(RuleInfo),
}

impl Rule {
    fn split(&self, parts_range: &PartsRange) -> ((Target, PartsRange), PartsRange) {
        match self {
            Rule::BasicRule(target) => (
                (target.clone(), parts_range.clone()),
                PartsRange {
                    x: 0..0,
                    m: 0..0,
                    a: 0..0,
                    s: 0..0,
                },
            ),
            Rule::ConditionalRule(rule_info) => {
                let mut applied = parts_range.clone();
                let mut rest = parts_range.clone();

                match rule_info.category {
                    Category::ExtremelyCoolLooking => match rule_info.condition {
                        Condition::GreaterThan => {
                            applied.x.start = rule_info.threshold + 1;
                            rest.x.end = rule_info.threshold + 1;
                        }
                        Condition::LessThan => {
                            applied.x.end = rule_info.threshold;
                            rest.x.start = rule_info.threshold;
                        }
                    },
                    Category::Musical => match rule_info.condition {
                        Condition::GreaterThan => {
                            applied.m.start = rule_info.threshold + 1;
                            rest.m.end = rule_info.threshold + 1;
                        }
                        Condition::LessThan => {
                            applied.m.end = rule_info.threshold;
                            rest.m.start = rule_info.threshold;
                        }
                    },
                    Category::Aerodynamic => match rule_info.condition {
                        Condition::GreaterThan => {
                            applied.a.start = rule_info.threshold + 1;
                            rest.a.end = rule_info.threshold + 1;
                        }
                        Condition::LessThan => {
                            applied.a.end = rule_info.threshold;
                            rest.a.start = rule_info.threshold;
                        }
                    },
                    Category::Shiny => match rule_info.condition {
                        Condition::GreaterThan => {
                            applied.s.start = rule_info.threshold + 1;
                            rest.s.end = rule_info.threshold + 1;
                        }
                        Condition::LessThan => {
                            applied.s.end = rule_info.threshold;
                            rest.s.start = rule_info.threshold;
                        }
                    },
                }

                ((rule_info.target.clone(), applied), rest)
            }
        }
    }

    fn applies(&self, part: &Part) -> Option<Target> {
        match self {
            Rule::BasicRule(target) => Some(target.clone()),
            Rule::ConditionalRule(rule_info) => {
                let part_value = match rule_info.category {
                    Category::ExtremelyCoolLooking => part.x,
                    Category::Musical => part.m,
                    Category::Aerodynamic => part.a,
                    Category::Shiny => part.s,
                };

                if match rule_info.condition {
                    Condition::GreaterThan => part_value > rule_info.threshold,
                    Condition::LessThan => part_value < rule_info.threshold,
                } {
                    Some(rule_info.target.clone())
                } else {
                    None
                }
            }
        }
    }

    fn parse(value: &str) -> anyhow::Result<Self> {
        let rule = if let Some((comparison, target)) = value.split_once(":") {
            let mut comparison_chars = comparison.chars();

            let category = comparison_chars.next().context("Wrong rule input")?;

            let category = match category {
                'x' => Category::ExtremelyCoolLooking,
                'm' => Category::Musical,
                'a' => Category::Aerodynamic,
                's' => Category::Shiny,
                _ => bail!("Wrong category input"),
            };

            let condition = comparison_chars.next().context("Wrong rule input")?;

            let condition = match condition {
                '>' => Condition::GreaterThan,
                '<' => Condition::LessThan,
                _ => bail!("Wrong condition input"),
            };

            let threshold = comparison_chars
                .as_str()
                .parse()
                .context("Wrong threshold")?;

            let target = match target {
                "A" => Target::Accept,
                "R" => Target::Reject,
                t => Target::Workflow(t.to_string()),
            };

            Rule::ConditionalRule(RuleInfo {
                category,
                condition,
                threshold,
                target,
            })
        } else {
            let target = match value {
                "A" => Target::Accept,
                "R" => Target::Reject,
                t => Target::Workflow(t.to_string()),
            };

            Rule::BasicRule(target)
        };

        Ok(rule)
    }
}

#[derive(Debug)]
struct Workflow {
    rules: Vec<Rule>,
}

impl Workflow {
    fn parse(value: &str) -> anyhow::Result<Self> {
        let mut rules = Vec::new();

        for rule in value.split(",") {
            let rule = Rule::parse(rule)?;

            rules.push(rule);
        }

        Ok(Self { rules })
    }
}

#[derive(Debug)]
pub struct System {
    workflows: HashMap<String, Workflow>,
    parts: Vec<Part>,
}

impl System {
    pub fn score2(&self) -> usize {
        let mut sum = 0;

        let accepted_ranges = self.accepted_combinations();

        for range in accepted_ranges {
            sum += range.x.into_iter().len()
                * range.m.into_iter().len()
                * range.a.into_iter().len()
                * range.s.into_iter().len();
        }

        sum
    }

    fn accepted_combinations(&self) -> Vec<PartsRange> {
        let mut accepted_ranges = Vec::new();
        let mut to_process = VecDeque::new();

        to_process.push_back((
            "in".to_string(),
            PartsRange {
                x: 1..4001,
                m: 1..4001,
                a: 1..4001,
                s: 1..4001,
            },
        ));

        while let Some((workflow_name, parts_range)) = to_process.pop_front() {
            if let Some(workflow) = self.workflows.get(&workflow_name) {
                let mut parts_range = parts_range;

                for rule in &workflow.rules {
                    let ((next_target, applied), rest) = rule.split(&parts_range);

                    if !applied.x.is_empty()
                        && !applied.m.is_empty()
                        && !applied.a.is_empty()
                        && !applied.s.is_empty()
                    {
                        match next_target {
                            Target::Accept => {
                                accepted_ranges.push(applied);
                            }
                            Target::Reject => {}
                            Target::Workflow(next_target) => {
                                to_process.push_back((next_target, applied));
                            }
                        }
                    }

                    if !rest.x.is_empty()
                        && !rest.m.is_empty()
                        && !rest.a.is_empty()
                        && !rest.s.is_empty()
                    {
                        parts_range = rest;
                    } else {
                        break;
                    }
                }
            }
        }

        accepted_ranges
    }

    pub fn score(&self) -> usize {
        let mut sum = 0;

        for part in &self.parts {
            if self.is_part_accepted(part) {
                sum += part.x + part.m + part.a + part.s;
            }
        }

        sum
    }

    fn is_part_accepted(&self, part: &Part) -> bool {
        let mut target = "in".to_string();

        loop {
            if let Some(workflow) = self.workflows.get(&target) {
                for rule in &workflow.rules {
                    if let Some(next_target) = rule.applies(part) {
                        match next_target {
                            Target::Accept => {
                                return true;
                            }
                            Target::Reject => {
                                return false;
                            }
                            Target::Workflow(next_target) => {
                                target = next_target;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    pub fn parse(input_data: String) -> anyhow::Result<Self> {
        let mut workflows = HashMap::new();
        let mut parts = Vec::new();

        let (workflows_input, parts_input) =
            input_data.split_once("\n\n").context("Wrong input data")?;

        for line in workflows_input.lines() {
            let (name, rules) = line.split_once("{").context("Wrong workflow data")?;

            let rules = rules.strip_suffix("}").context("Wrong workflow suffix")?;

            let workflow = Workflow::parse(rules)?;

            workflows.insert(name.to_string(), workflow);
        }

        for line in parts_input.lines() {
            let part = Part::parse(line)?;

            parts.push(part);
        }

        Ok(Self { workflows, parts })
    }
}
