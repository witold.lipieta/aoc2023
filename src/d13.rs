use std::cmp::min;
use std::collections::HashSet;
use std::iter::zip;

#[derive(Debug, Hash, Eq, PartialEq, Clone)]
enum Reflection {
    Horizontal(usize),
    Vertical(usize),
}

#[derive(Debug)]
pub struct Pattern {
    rocks: Vec<Vec<bool>>,
}

impl Pattern {
    fn get_reflections(&self) -> HashSet<Reflection> {
        let mut reflections = HashSet::new();

        for row_index in 1..self.rocks.len() {
            if self.is_horizontal_reflection(row_index) {
                reflections.insert(Reflection::Horizontal(row_index));
            }
        }

        for col_index in 1..self.rocks[0].len() {
            if self.is_vertical_reflection(col_index) {
                reflections.insert(Reflection::Vertical(col_index));
            }
        }

        reflections
    }

    fn score_reflections(reflections: HashSet<Reflection>) -> usize {
        reflections
            .iter()
            .map(|reflection| match reflection {
                Reflection::Vertical(index) => *index,
                Reflection::Horizontal(index) => *index * 100,
            })
            .sum()
    }

    pub fn score(&self) -> usize {
        let reflections = self.get_reflections();

        Self::score_reflections(reflections)
    }

    fn smudge_repaired(&self, row: usize, col: usize) -> Self {
        let mut rocks = self.rocks.clone();

        rocks[row][col] ^= true;

        Self { rocks }
    }

    pub fn score2(&self) -> usize {
        let reflections = self.get_reflections();

        for row_index in 0..self.rocks.len() {
            for col_index in 0..self.rocks[0].len() {
                let repaired = self.smudge_repaired(row_index, col_index);

                let repaired_reflections = repaired.get_reflections();

                if !repaired_reflections.is_empty() && repaired_reflections != reflections {
                    return Self::score_reflections(
                        repaired_reflections
                            .difference(&reflections)
                            .map(|reflection| reflection.clone())
                            .collect(),
                    );
                }
            }
        }

        0
    }

    fn is_horizontal_reflection(&self, row: usize) -> bool {
        let rows_to_compare = min((0..row).len(), (row..self.rocks.len()).len());

        for (first, second) in zip(
            self.rocks[row - rows_to_compare..row].iter().rev(),
            self.rocks[row..row + rows_to_compare].iter(),
        ) {
            if first != second {
                return false;
            }
        }

        true
    }

    fn is_vertical_reflection(&self, col: usize) -> bool {
        let cols_to_compare = min((0..col).len(), (col..self.rocks[0].len()).len());

        for (first_col, second_col) in zip(
            (col - cols_to_compare..col).into_iter().rev(),
            (col..col + cols_to_compare).into_iter(),
        ) {
            let first = self
                .rocks
                .iter()
                .map(|row| row[first_col])
                .collect::<Vec<_>>();

            let second = self
                .rocks
                .iter()
                .map(|row| row[second_col])
                .collect::<Vec<_>>();

            if first != second {
                return false;
            }
        }

        true
    }

    pub fn parse(input: &str) -> anyhow::Result<Self> {
        let mut rocks = Vec::new();

        for row in input.lines() {
            let row_pattern = row.chars().map(|symbol| symbol == '#').collect::<Vec<_>>();

            rocks.push(row_pattern)
        }

        Ok(Self { rocks })
    }
}
