use anyhow::{Context, Result};

const BASE_DECIMAL: u32 = 10;

#[derive(Clone)]
enum DigitWord {
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
}

impl DigitWord {
    fn to_string(&self) -> String {
        match self {
            DigitWord::One => "one",
            DigitWord::Two => "two",
            DigitWord::Three => "three",
            DigitWord::Four => "four",
            DigitWord::Five => "five",
            DigitWord::Six => "six",
            DigitWord::Seven => "seven",
            DigitWord::Eight => "eight",
            DigitWord::Nine => "nine",
        }
        .to_string()
    }

    fn to_string_reversed(&self) -> String {
        self.to_string().chars().rev().collect::<String>()
    }
}

impl Into<u32> for DigitWord {
    fn into(self) -> u32 {
        match self {
            DigitWord::One => 1,
            DigitWord::Two => 2,
            DigitWord::Three => 3,
            DigitWord::Four => 4,
            DigitWord::Five => 5,
            DigitWord::Six => 6,
            DigitWord::Seven => 7,
            DigitWord::Eight => 8,
            DigitWord::Nine => 9,
        }
    }
}

enum Direction {
    FromStart,
    FromEnd,
}

#[derive(Clone)]
struct Digit {
    index: usize,
    digit: u32,
}

impl Digit {
    fn new(index: usize, digit: u32) -> Self {
        Self { index, digit }
    }

    fn find_first(line: &str, direction: Direction) -> Result<Digit> {
        let digit_words = vec![
            DigitWord::One,
            DigitWord::Two,
            DigitWord::Three,
            DigitWord::Four,
            DigitWord::Five,
            DigitWord::Six,
            DigitWord::Seven,
            DigitWord::Eight,
            DigitWord::Nine,
        ];

        let line = match direction {
            Direction::FromStart => line.to_string(),
            Direction::FromEnd => line.chars().rev().collect::<String>(),
        };

        let mut digits_found = digit_words
            .iter()
            .filter_map(|digit_word| {
                let digit_string = match direction {
                    Direction::FromStart => digit_word.to_string(),
                    Direction::FromEnd => digit_word.to_string_reversed(),
                };

                let index = line.find(digit_string.as_str())?;

                Some(Digit::new(index, digit_word.clone().into()))
            })
            .collect::<Vec<_>>();

        if let Some(digit_found) = line
            .chars()
            .position(|c| c.is_digit(BASE_DECIMAL))
            .map(|index| {
                line.chars()
                    .nth(index)
                    .map(|c| c.to_digit(BASE_DECIMAL).map(|d| Digit::new(index, d)))
            })
            .flatten()
            .flatten()
        {
            digits_found.push(digit_found);
        }

        digits_found.sort_by_key(|df| df.index);

        digits_found
            .first()
            .map(|df| df.clone())
            .context("No digits found")
    }

    fn find_from_start(line: &str) -> Result<u32> {
        let found_digit = Self::find_first(line, Direction::FromStart)?;

        Ok(found_digit.digit)
    }

    fn find_from_end(line: &str) -> Result<u32> {
        let found_digit = Self::find_first(line, Direction::FromEnd)?;

        Ok(found_digit.digit)
    }
}

pub struct TwoDigitNumber {
    first_digit: u32,
    second_digit: u32,
}

impl TwoDigitNumber {
    pub fn parse(line: &str) -> Result<Self> {
        let first_digit = line
            .chars()
            .find_map(|c| c.to_digit(BASE_DECIMAL))
            .context("No decimal digits found")?;

        let second_digit = line
            .chars()
            .rev()
            .find_map(|c| c.to_digit(BASE_DECIMAL))
            .context("No decimal digits found")?;

        Ok(Self {
            first_digit,
            second_digit,
        })
    }

    pub fn parse2(line: &str) -> Result<Self> {
        let first_digit = Digit::find_from_start(line)?;

        let second_digit = Digit::find_from_end(line)?;

        Ok(Self {
            first_digit,
            second_digit,
        })
    }
}

impl Into<u32> for &TwoDigitNumber {
    fn into(self) -> u32 {
        self.first_digit * BASE_DECIMAL + self.second_digit
    }
}
