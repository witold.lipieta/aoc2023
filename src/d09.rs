use anyhow::{bail, Context, Error};

#[derive(Debug)]
struct SensorReadings {
    history: Vec<i64>,
}

impl SensorReadings {
    fn differences(&self) -> Self {
        let mut history = Vec::new();

        for index in 1..self.history.len() {
            let difference = self.history[index] - self.history[index - 1];

            history.push(difference)
        }

        Self { history }
    }

    fn is_zeros(&self) -> bool {
        self.history.iter().filter(|reading| **reading == 0).count() == self.history.len()
    }

    fn get_next_prediction(&self) -> anyhow::Result<i64> {
        if self.is_zeros() {
            Ok(0)
        } else {
            match self.history.last() {
                Some(last_reading) => Ok(*last_reading + self.differences().get_next_prediction()?),
                None => bail!("No history readings"),
            }
        }
    }

    fn get_previous_prediction(&self) -> anyhow::Result<i64> {
        if self.is_zeros() {
            Ok(0)
        } else {
            match self.history.first() {
                Some(first_reading) => {
                    Ok(*first_reading - self.differences().get_previous_prediction()?)
                }
                None => bail!("No history readings"),
            }
        }
    }
}

impl TryFrom<&str> for SensorReadings {
    type Error = Error;

    fn try_from(value: &str) -> std::result::Result<Self, Self::Error> {
        let history = value
            .split_whitespace()
            .map(|reading| reading.parse().context("Wrong input"))
            .collect::<anyhow::Result<_>>()?;

        Ok(Self { history })
    }
}

pub struct Oasis {
    data_sets: Vec<SensorReadings>,
}

impl Oasis {
    pub fn score(&self) -> anyhow::Result<i64> {
        Ok(self
            .data_sets
            .iter()
            .map(|sensor_readings| sensor_readings.get_next_prediction())
            .collect::<anyhow::Result<Vec<i64>>>()?
            .into_iter()
            .sum())
    }

    pub fn score2(&self) -> anyhow::Result<i64> {
        Ok(self
            .data_sets
            .iter()
            .map(|sensor_readings| sensor_readings.get_previous_prediction())
            .collect::<anyhow::Result<Vec<i64>>>()?
            .into_iter()
            .sum())
    }

    pub fn parse(lines: Vec<String>) -> anyhow::Result<Self> {
        let data_sets = lines
            .iter()
            .map(|line| line.as_str().try_into().context("Wrong input"))
            .collect::<anyhow::Result<_>>()?;

        Ok(Self { data_sets })
    }
}
