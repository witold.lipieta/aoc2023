use anyhow::bail;
use std::collections::{BTreeSet, HashSet};
use std::fmt::{Display, Formatter};
use std::ops::Range;

#[derive(Debug)]
pub struct Platform {
    rows: usize,
    cols: usize,
    v_rocks: Vec<BTreeSet<usize>>,
    h_rocks: Vec<BTreeSet<usize>>,
    v_ranges: Vec<Vec<Range<usize>>>,
    h_ranges: Vec<Vec<Range<usize>>>,
}

impl Platform {
    pub fn total_load(&mut self) -> usize {
        let mut sum = 0;

        for row in 0..self.rows {
            for _ in &self.h_rocks[row] {
                sum += self.rows - row;
            }
        }

        for col in 0..self.cols {
            for row in &self.v_rocks[col] {
                sum += self.rows - *row;
            }
        }

        sum
    }

    pub fn cycle(&mut self, cycles: usize) {
        let mut stored = None;
        let mut last_match = 0;
        let mut cycle = 0;

        while cycle < cycles {
            self.tilt_north();
            self.tilt_west();
            self.tilt_south();
            self.tilt_east();

            if stored.is_none() && cycle == 1000 {
                stored = Some(self.v_rocks.clone());
                last_match = cycle;
            }

            if cycle > 1000 {
                if let Some(stored) = &stored {
                    if &self.v_rocks == stored {
                        let match_loop = cycle - last_match;
                        let cycles_left = cycles - 1 - cycle;
                        let matches_left = cycles_left / match_loop;
                        cycle += matches_left * match_loop;
                    }
                }
            }

            cycle += 1;
        }
    }

    fn tilt_east(&mut self) {
        for row in 0..self.rows {
            for range in &self.h_ranges[row] {
                let rocks_in_range = self.h_rocks[row].range(range.clone()).count();

                for rock_i in 0..rocks_in_range {
                    self.v_rocks[range.end - 1 - rock_i].insert(row);
                }
            }

            self.h_rocks[row].clear();
        }
    }

    fn tilt_south(&mut self) {
        for col in 0..self.cols {
            for range in &self.v_ranges[col] {
                let rocks_in_range = self.v_rocks[col].range(range.clone()).count();

                for rock_i in 0..rocks_in_range {
                    self.h_rocks[range.end - 1 - rock_i].insert(col);
                }
            }

            self.v_rocks[col].clear();
        }
    }

    fn tilt_west(&mut self) {
        for row in 0..self.rows {
            for range in &self.h_ranges[row] {
                let rocks_in_range = self.h_rocks[row].range(range.clone()).count();

                for rock_i in 0..rocks_in_range {
                    self.v_rocks[range.start + rock_i].insert(row);
                }
            }

            self.h_rocks[row].clear();
        }
    }

    pub fn tilt_north(&mut self) {
        for col in 0..self.cols {
            for range in &self.v_ranges[col] {
                let rocks_in_range = self.v_rocks[col].range(range.clone()).count();

                for rock_i in 0..rocks_in_range {
                    self.h_rocks[range.start + rock_i].insert(col);
                }
            }

            self.v_rocks[col].clear();
        }
    }

    pub fn parse(lines: Vec<String>) -> anyhow::Result<Self> {
        if lines.is_empty() {
            bail!("Wrong input data");
        }

        let rows = lines.len();
        let cols = lines[0].len();

        let mut v_rocks = vec![BTreeSet::new(); cols];
        let h_rocks = vec![BTreeSet::new(); rows];

        let mut v_ranges = vec![Vec::new(); cols];
        let mut h_ranges = vec![Vec::new(); rows];

        let mut v_range_start = vec![0; cols];
        let mut h_range_start = vec![0; rows];

        for row in 0..rows {
            for (col, symbol) in lines[row].chars().enumerate() {
                match symbol {
                    'O' => {
                        v_rocks[col].insert(row);
                    }
                    '#' => {
                        let v_range = v_range_start[col]..row;

                        if !v_range.is_empty() {
                            v_ranges[col].push(v_range);
                        }

                        v_range_start[col] = row + 1;

                        let h_range = h_range_start[row]..col;

                        if !h_range.is_empty() {
                            h_ranges[row].push(h_range);
                        }

                        h_range_start[row] = col + 1;
                    }
                    _ => {}
                }
            }
        }

        for col in 0..cols {
            let v_range = v_range_start[col]..rows;

            if !v_range.is_empty() {
                v_ranges[col].push(v_range);
            }
        }

        for row in 0..rows {
            let h_range = h_range_start[row]..cols;

            if !h_range.is_empty() {
                h_ranges[row].push(h_range);
            }
        }

        Ok(Self {
            rows,
            cols,
            v_rocks,
            h_rocks,
            v_ranges,
            h_ranges,
        })
    }
}

impl Display for Platform {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut cube_rocks = HashSet::new();
        let mut rounded_rocks = HashSet::new();

        for col in 0..self.cols {
            for range in &self.v_ranges[col] {
                if range.start > 0 {
                    cube_rocks.insert((range.start - 1, col));
                }
                if range.end < self.rows {
                    cube_rocks.insert((range.end, col));
                }
            }
        }

        for row in 0..self.rows {
            for col in &self.h_rocks[row] {
                rounded_rocks.insert((row, *col));
            }
        }
        for col in 0..self.cols {
            for row in &self.v_rocks[col] {
                rounded_rocks.insert((*row, col));
            }
        }

        for row in 0..self.rows {
            for col in 0..self.cols {
                if rounded_rocks.contains(&(row, col)) {
                    write!(f, "O")?;
                } else if cube_rocks.contains(&(row, col)) {
                    write!(f, "#")?;
                } else {
                    write!(f, ".")?;
                }
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}
