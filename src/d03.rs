use anyhow::{bail, Context};
use std::collections::HashSet;

const BASE_DECIMAL: u32 = 10;

const SKIP_SYMBOL: char = '.';
const GEAR_SYMBOL: char = '*';

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
struct SchematicNumber {
    row: usize,
    index: usize,
    len: usize,
    value: u32,
}

impl SchematicNumber {
    fn new(row: usize, index: usize, number: &str) -> anyhow::Result<Self> {
        let len = number.len();
        let value = number.parse().context("Failed to parse number")?;

        Ok(Self {
            row,
            index,
            len,
            value,
        })
    }

    fn is_adjacent(&self, row: usize, index: usize) -> bool {
        row.abs_diff(self.row) <= 1 && index + 1 >= self.index && index <= self.index + self.len
    }
}

#[derive(Debug)]
pub struct EngineSchematic {
    rows: usize,
    cols: usize,
    numbers: Vec<Vec<SchematicNumber>>,
    parts: HashSet<SchematicNumber>,
    gear_ratios: Vec<u32>,
}

impl EngineSchematic {
    pub fn new(schematic: Vec<String>) -> anyhow::Result<Self> {
        if schematic.is_empty() {
            bail!("Input data is empty")
        }

        let rows = schematic.len();

        let cols = schematic[0].len();

        let numbers = vec![Vec::new(); rows];

        let mut engine_schematic = Self {
            rows,
            cols,
            numbers,
            parts: HashSet::new(),
            gear_ratios: Vec::new(),
        };

        engine_schematic.parse_numbers(&schematic)?;

        engine_schematic.parse_symbols(&schematic)?;

        Ok(engine_schematic)
    }

    fn parse_numbers(&mut self, schematic: &Vec<String>) -> anyhow::Result<()> {
        for (row_index, row) in schematic.iter().enumerate() {
            let mut part_number = String::new();

            let mut part_number_index = 0;

            for (col_index, symbol) in row.chars().enumerate() {
                if symbol.is_digit(BASE_DECIMAL) {
                    if part_number.is_empty() {
                        part_number_index = col_index;
                    }

                    part_number.push(symbol);
                } else if !part_number.is_empty() {
                    self.numbers[row_index].push(SchematicNumber::new(
                        row_index,
                        part_number_index,
                        part_number.as_str(),
                    )?);

                    part_number.clear();
                }
            }

            if !part_number.is_empty() {
                self.numbers[row_index].push(SchematicNumber::new(
                    row_index,
                    part_number_index,
                    part_number.as_str(),
                )?);
            }
        }

        Ok(())
    }

    fn parse_symbols(&mut self, schematic: &Vec<String>) -> anyhow::Result<()> {
        for (row_index, row) in schematic.iter().enumerate() {
            for (col_index, symbol) in row.chars().enumerate() {
                if symbol != SKIP_SYMBOL && !symbol.is_digit(BASE_DECIMAL) {
                    self.add_symbol(row_index, col_index, symbol)?;
                }
            }
        }

        Ok(())
    }

    fn add_symbol(&mut self, row: usize, col: usize, symbol: char) -> anyhow::Result<()> {
        if row >= self.rows || col >= self.cols {
            bail!("Symbol is out of schematic bounds");
        }

        let mut adjacent_numbers = Vec::new();

        for number in &self.numbers[row] {
            if number.is_adjacent(row, col) {
                adjacent_numbers.push(number.clone());
            }
        }

        if row > 0 {
            for number in &self.numbers[row - 1] {
                if number.is_adjacent(row, col) {
                    adjacent_numbers.push(number.clone());
                }
            }
        }

        if row + 1 < self.rows {
            for number in &self.numbers[row + 1] {
                if number.is_adjacent(row, col) {
                    adjacent_numbers.push(number.clone());
                }
            }
        }

        if symbol == GEAR_SYMBOL && adjacent_numbers.len() == 2 {
            self.gear_ratios
                .push(adjacent_numbers[0].value * adjacent_numbers[1].value)
        }

        for number in adjacent_numbers {
            self.parts.insert(number);
        }

        Ok(())
    }

    pub fn parts(&self) -> impl Iterator<Item = u32> + '_ {
        self.parts
            .iter()
            .map(|schematic_number| schematic_number.value)
    }

    pub fn gear_ratios(&self) -> impl Iterator<Item = u32> + '_ {
        self.gear_ratios.iter().map(|gr| *gr)
    }
}
