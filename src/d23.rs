use anyhow::{bail, Error};
use std::collections::{HashMap, HashSet, VecDeque};
use std::fmt::{Display, Formatter};

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
enum Direction {
    Up,
    Down,
    Right,
    Left,
}

impl Direction {
    fn next(&self, row: usize, col: usize) -> (usize, usize) {
        match self {
            Direction::Up => (row - 1, col),
            Direction::Down => (row + 1, col),
            Direction::Right => (row, col + 1),
            Direction::Left => (row, col - 1),
        }
    }
}

type Node = (usize, usize);

#[derive(Debug)]
enum Tile {
    Path,
    Forest,
    Slope(Direction),
}

impl TryFrom<char> for Tile {
    type Error = Error;

    fn try_from(value: char) -> std::result::Result<Self, Self::Error> {
        let tile = match value {
            '.' => Self::Path,
            '#' => Self::Forest,
            '^' => Self::Slope(Direction::Up),
            'v' => Self::Slope(Direction::Down),
            '>' => Self::Slope(Direction::Right),
            '<' => Self::Slope(Direction::Left),
            _ => {
                bail!("Wrong tile symbol")
            }
        };

        Ok(tile)
    }
}

#[derive(Debug)]
pub struct Map {
    map: Vec<Vec<Tile>>,
}

impl Map {
    pub fn starting_node(&self) -> Node {
        (0, 1)
    }

    pub fn ending_node(&self) -> Node {
        (self.map.len() - 1, self.map[0].len() - 2)
    }

    fn get_next_nodes2(
        &mut self,
        current: Node,
        prev_dir: Direction,
        current_length: i64,
    ) -> (Node, i64, Vec<Direction>) {
        let end_node = self.ending_node();
        self.map[0][1] = Tile::Forest;

        if current == end_node {
            return (
                self.ending_node(),
                current_length,
                Vec::from([Direction::Down]),
            );
        }

        let next_dirs = match prev_dir {
            Direction::Up => [Direction::Up, Direction::Right, Direction::Left],
            Direction::Down => [Direction::Down, Direction::Right, Direction::Left],
            Direction::Right => [Direction::Right, Direction::Up, Direction::Down],
            Direction::Left => [Direction::Left, Direction::Up, Direction::Down],
        };

        let mut possible_dirs = Vec::new();

        for dir in next_dirs {
            let next = dir.next(current.0, current.1);
            match self.map[next.0][next.1] {
                Tile::Path => {
                    possible_dirs.push(dir);
                }
                Tile::Slope(_) => {
                    possible_dirs.push(dir);
                }
                _ => {}
            }
        }

        if possible_dirs.len() == 1 {
            let direction = possible_dirs[0];
            let next = direction.next(current.0, current.1);

            self.get_next_nodes2(next, direction, current_length + 1)
        } else {
            (current, current_length, possible_dirs)
        }
    }

    fn get_next_nodes(
        &self,
        current: Node,
        prev_dir: Direction,
        current_length: i64,
    ) -> Vec<(Node, Direction, i64)> {
        let end_node = self.ending_node();

        if current == end_node {
            return Vec::from([(self.ending_node(), Direction::Down, current_length)]);
        }

        let next_dirs = match prev_dir {
            Direction::Up => [Direction::Up, Direction::Right, Direction::Left],
            Direction::Down => [Direction::Down, Direction::Right, Direction::Left],
            Direction::Right => [Direction::Right, Direction::Up, Direction::Down],
            Direction::Left => [Direction::Left, Direction::Up, Direction::Down],
        };

        let mut next_nodes = Vec::new();

        for dir in next_dirs {
            let next = dir.next(current.0, current.1);
            match self.map[next.0][next.1] {
                Tile::Path => {
                    next_nodes.append(&mut self.get_next_nodes(next, dir, current_length + 1))
                }
                Tile::Slope(slope_dir) if dir == slope_dir => {
                    next_nodes.push((next, dir, current_length));
                }
                _ => {}
            }
        }

        next_nodes
    }

    pub fn parse(input: Vec<String>) -> anyhow::Result<Self> {
        let map = input
            .into_iter()
            .map(|row| {
                row.chars()
                    .map(|symbol| symbol.try_into())
                    .collect::<anyhow::Result<Vec<_>>>()
            })
            .collect::<anyhow::Result<Vec<_>>>()?;

        Ok(Self { map })
    }
}

#[derive(Debug)]
pub struct Graph {
    nodes: HashSet<Node>,
    edges: HashMap<Node, HashMap<Node, i64>>,
}

impl Graph {
    pub fn calculate_longest_path2(&self, start: Node, goal: Node) -> i64 {
        let mut paths_to_process = VecDeque::new();
        let mut paths: HashMap<Node, (Vec<Node>, i64)> = HashMap::new();

        paths_to_process.push_back((Vec::from([start]), 0));

        while let Some((path, path_distance)) = paths_to_process.pop_front() {
            let node = path.last().unwrap().clone();

            if node == goal {
                continue;
            }

            for (neighbour, neighbour_distance) in self.edges.get(&node).cloned().unwrap() {
                if path.contains(&neighbour) {
                    continue;
                }

                let mut new_path = path.clone();
                new_path.push(neighbour);
                let new_distance = path_distance + neighbour_distance;

                paths
                    .entry(neighbour)
                    .and_modify(|(path, distance)| {
                        if *distance < new_distance {
                            *path = new_path.clone();
                            *distance = new_distance;
                        }
                    })
                    .or_insert((new_path.clone(), new_distance));

                paths_to_process.push_back((new_path, new_distance));
            }
        }

        paths.get(&goal).unwrap().1
    }

    pub fn calculate_distances(
        &self,
        mut nodes: VecDeque<Node>,
        start: Node,
        goal: Node,
    ) -> HashMap<Node, i64> {
        let mut distances = nodes
            .iter()
            .map(|n| (n.clone(), i64::MIN))
            .collect::<HashMap<Node, i64>>();

        distances.insert(start, 0);

        while let Some(node) = nodes.pop_front() {
            let current_distance = distances.get(&node).cloned().unwrap();

            if node == goal {
                continue;
            }

            if current_distance != i64::MIN {
                for (neighbour, distance) in self.edges.get(&node).cloned().unwrap() {
                    let neighbour_distance = distances.get(&neighbour).cloned().unwrap();
                    if neighbour_distance < current_distance + distance {
                        distances.insert(neighbour, current_distance + distance);
                    }
                }
            }
        }

        distances
    }

    pub fn kahn(&self, start: Node) -> VecDeque<Node> {
        let mut edges = self.edges.clone();
        let mut sorted = VecDeque::new();
        let mut stack = VecDeque::new();

        stack.push_back(start);

        while let Some(node) = stack.pop_front() {
            sorted.push_back(node);

            if let Some(edges_from_node) = edges.remove(&node) {
                for (neighbour, _) in edges_from_node {
                    let incoming_edges = edges
                        .iter()
                        .filter(|(_, hm)| hm.contains_key(&neighbour))
                        .map(|(n, _)| n.clone())
                        .collect::<Vec<_>>();

                    if incoming_edges.is_empty() {
                        stack.push_back(neighbour);
                    }
                }
            }
        }

        sorted
    }

    pub fn read_map(map: &Map) -> Self {
        let starting_node = map.starting_node();
        let ending_node = map.ending_node();

        let mut nodes = HashSet::new();
        let mut edges: HashMap<Node, HashMap<Node, i64>> = HashMap::new();

        let mut nodes_to_check = VecDeque::new();

        nodes_to_check.push_back((starting_node, Direction::Down));

        while let Some((current_node, current_direction)) = nodes_to_check.pop_front() {
            if nodes.insert(current_node) {
                if current_node == ending_node {
                    continue;
                }

                for (next_node, next_direction, length) in
                    map.get_next_nodes(current_node, current_direction, 1)
                {
                    if next_node != ending_node {
                        nodes_to_check.push_back((next_node, next_direction));
                    } else {
                        nodes.insert(ending_node);
                    }

                    edges
                        .entry(current_node)
                        .and_modify(|v| {
                            v.insert(next_node, length);
                        })
                        .or_insert(HashMap::from([(next_node, length)]));
                }
            }
        }

        Self { nodes, edges }
    }

    pub fn read_map2(map: &mut Map) -> Self {
        let starting_node = map.starting_node();
        let ending_node = map.ending_node();

        let mut nodes = HashSet::new();
        let mut visited_paths = HashSet::new();
        let mut edges: HashMap<Node, HashMap<Node, i64>> = HashMap::new();

        let mut nodes_to_check = VecDeque::new();

        nodes_to_check.push_back((starting_node, starting_node, Direction::Down));

        while let Some((current_node, path_start, current_direction)) = nodes_to_check.pop_front() {
            nodes.insert(current_node);

            if current_node == ending_node {
                continue;
            }

            let (next_node, length, directions) =
                map.get_next_nodes2(path_start, current_direction, 1);

            if !directions.is_empty() {
                edges
                    .entry(current_node)
                    .and_modify(|v| {
                        v.insert(next_node, length);
                    })
                    .or_insert(HashMap::from([(next_node, length)]));
            }

            for next_direction in directions {
                let next_path_start = next_direction.next(next_node.0, next_node.1);

                if next_node != ending_node {
                    if visited_paths.insert(next_path_start) {
                        nodes_to_check.push_back((next_node, next_path_start, next_direction));
                    }
                } else {
                    nodes.insert(ending_node);
                }
            }
        }

        Self { nodes, edges }
    }
}

impl Display for Graph {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for node in &self.nodes {
            write!(f, "{:?}\n", node)?;

            if let Some(edge) = self.edges.get(node) {
                write!(f, "\t{:?}\n", edge)?;
            }
        }

        Ok(())
    }
}
