use anyhow::{bail, Context, Error};
use std::collections::{BTreeMap, HashSet};
use std::fmt::{Display, Formatter};

#[derive(Debug, Clone, PartialEq)]
enum SpringCondition {
    Operational,
    Damaged,
    Unknown,
}

impl Display for SpringCondition {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let symbol = match self {
            SpringCondition::Operational => '.',
            SpringCondition::Damaged => '#',
            SpringCondition::Unknown => '?',
        };

        write!(f, "{}", symbol)
    }
}

impl TryFrom<char> for SpringCondition {
    type Error = Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        let spring_condition = match value {
            '.' => Self::Operational,
            '#' => Self::Damaged,
            '?' => Self::Unknown,
            _ => bail!("Wrong spring condition symbol"),
        };

        Ok(spring_condition)
    }
}

#[derive(Debug, Clone)]
struct Pattern {
    springs: Vec<SpringCondition>,
}

impl Pattern {
    fn from(springs: Vec<SpringCondition>) -> Self {
        Self { springs }
    }

    fn len(&self) -> usize {
        self.springs.len()
    }

    fn matches(&self, other: &Self) -> bool {
        for offset in 0..other.len() {
            match (&self.springs[offset], &other.springs[offset]) {
                (SpringCondition::Operational, SpringCondition::Damaged)
                | (SpringCondition::Damaged, SpringCondition::Operational) => {
                    return false;
                }
                (_, _) => {}
            }
        }

        true
    }
}

impl TryFrom<&str> for Pattern {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let springs = value
            .chars()
            .map(|symbol| symbol.try_into())
            .collect::<anyhow::Result<Vec<_>>>()?;

        Ok(Self { springs })
    }
}

impl Display for Pattern {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for spring in &self.springs {
            write!(f, "{}", spring)?;
        }

        Ok(())
    }
}

#[derive(Debug, Clone)]
struct PatternCandidate {
    input_pattern: Pattern,
    input_groups: Vec<usize>,
    candidates: BTreeMap<usize, usize>,
    pattern: Pattern,
    input_pattern_groups: Vec<(usize, usize)>,
    reserved: Vec<bool>,
}

impl PatternCandidate {
    fn new(input_pattern: Pattern, input_groups: Vec<usize>) -> Self {
        let length = input_pattern.len();

        let mut input_pattern_groups = Vec::new();

        let mut current_pattern_group_index = 0;
        let mut current_pattern_group_length = 0;

        for pattern_index in 0..length {
            if input_pattern.springs[pattern_index] == SpringCondition::Damaged {
                if current_pattern_group_length == 0 {
                    current_pattern_group_index = pattern_index;
                }

                current_pattern_group_length += 1;
            } else if current_pattern_group_length != 0 {
                input_pattern_groups
                    .push((current_pattern_group_index, current_pattern_group_length));

                current_pattern_group_length = 0;
            }
        }

        Self {
            input_pattern,
            input_groups,
            candidates: BTreeMap::new(),
            pattern: Pattern::from(vec![SpringCondition::Unknown; length]),
            input_pattern_groups,
            reserved: vec![false; length],
        }
    }

    fn is_last(&self, group_index: usize) -> bool {
        group_index == self.input_groups.len() - 1
    }

    fn append(&mut self, group_index: usize, pattern_index: usize) -> bool {
        let pattern = match self.can_be_appended(group_index, pattern_index) {
            Some(pattern) => pattern,
            None => return false,
        };

        self.candidates.insert(group_index, pattern_index);

        self.pattern = pattern;

        for is_reserved in
            self.reserved[pattern_index..pattern_index + self.input_groups[group_index]].iter_mut()
        {
            *is_reserved = true;
        }

        true
    }

    fn can_be_appended(&self, group_index: usize, pattern_index: usize) -> Option<Pattern> {
        if self.reserved[pattern_index..pattern_index + self.input_groups[group_index]]
            .iter()
            .any(|is_reserved| *is_reserved)
        {
            return None;
        }

        let pattern = self.pattern_with(group_index, pattern_index);

        if !self.input_pattern.matches(&pattern) {
            return None;
        }

        Some(pattern)
    }

    fn pattern_with(&self, group_index: usize, pattern_index: usize) -> Pattern {
        let mut pattern = self.pattern.clone();

        if pattern_index != 0 {
            if group_index == 0 {
                // all springs before this group are operational
                for spring in pattern.springs[0..pattern_index].iter_mut() {
                    *spring = SpringCondition::Operational;
                }
            } else if let Some(previous_pattern_index) = self.candidates.get(&(group_index - 1)) {
                // all springs since previous group are operational
                let previous_group_index = group_index - 1;
                for spring in pattern.springs[previous_pattern_index
                    + self.input_groups[previous_group_index]
                    ..pattern_index]
                    .iter_mut()
                {
                    *spring = SpringCondition::Operational;
                }
            } else {
                // there is at least one operational spring prefix
                pattern.springs[pattern_index - 1] = SpringCondition::Operational;
            }
        }

        let group_count = self.input_groups[group_index];

        for spring in pattern.springs[pattern_index..pattern_index + group_count].iter_mut() {
            // all springs in this group are damaged
            *spring = SpringCondition::Damaged;
        }

        if self.is_last(group_index) {
            // all springs after this group are operational
            for spring in pattern.springs[pattern_index + group_count..].iter_mut() {
                *spring = SpringCondition::Operational;
            }
        } else if let Some(next_pattern_index) = self.candidates.get(&(group_index + 1)) {
            // all springs until nex group are operational
            for spring in
                pattern.springs[pattern_index + group_count..*next_pattern_index].iter_mut()
            {
                *spring = SpringCondition::Operational;
            }
        } else if pattern_index + group_count < self.input_pattern.len() {
            // there is at least one operational spring suffix
            pattern.springs[pattern_index + group_count] = SpringCondition::Operational;
        }

        pattern
    }

    fn min_limit(&self, group_index: usize) -> usize {
        if group_index == 0 {
            return 0;
        }

        let range = 0..group_index;

        self.input_groups[range.clone()].iter().sum::<usize>() + range.len()
    }

    fn max_limit(&self, group_index: usize) -> usize {
        let group_count = self.input_groups[group_index];

        if self.is_last(group_index) {
            return self.input_pattern.len() - group_count + 1;
        }

        let next_group_index = group_index + 1;

        let range = next_group_index..self.input_groups.len();

        self.input_pattern.len()
            - self.input_groups[range.clone()].iter().sum::<usize>()
            - range.len()
            - group_count
            + 1
    }
}

impl Display for PatternCandidate {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "PC: {} {:?} {:?}",
            self.pattern, self.input_pattern_groups, self.input_groups
        )
    }
}

#[derive(Debug)]
pub struct SpringRow {
    spring_conditions: Pattern,
    damaged_groups: Vec<usize>,
}

impl Display for SpringRow {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "SR: {} {:?}",
            self.spring_conditions, self.damaged_groups
        )
    }
}

impl SpringRow {
    pub fn arrangements(&self) -> usize {
        let possible_indexes = self.possible_indexes();

        let mut next = vec![Vec::new(); self.damaged_groups.len()];

        self.calc_pairs(&possible_indexes, &mut next);

        self.limit_pairs(&mut next);

        self.score(&next)
    }

    fn score(&self, next: &Vec<Vec<HashSet<usize>>>) -> usize {
        let mut scores = vec![Vec::new(); self.damaged_groups.len()];

        for _ in 0..next[self.damaged_groups.len() - 1].len() {
            scores[self.damaged_groups.len() - 1].push(1usize)
        }

        for gi in (0..self.damaged_groups.len() - 1).into_iter().rev() {
            for i in 0..next[gi].len() {
                let mut count = 0;

                for next_group in &next[gi][i] {
                    count += scores[gi + 1][*next_group];
                }

                scores[gi].push(count);
            }
        }

        scores[0].iter().sum()
    }

    fn limit_pairs(&self, next: &mut Vec<Vec<HashSet<usize>>>) {
        for gi in (1..self.damaged_groups.len() - 1).into_iter().rev() {
            for i in 0..next[gi].len() {
                if next[gi][i].is_empty() {
                    for next_groups in next[gi - 1].iter_mut() {
                        next_groups.remove(&i);
                    }
                }
            }
        }
    }

    fn calc_pairs(&self, possible_indexes: &Vec<Vec<usize>>, next: &mut Vec<Vec<HashSet<usize>>>) {
        let pattern =
            PatternCandidate::new(self.spring_conditions.clone(), self.damaged_groups.clone());
        for gi in 0..self.damaged_groups.len() {
            for (i, pattern_index_first) in possible_indexes[gi].iter().enumerate() {
                next[gi].push(HashSet::new());

                if gi == self.damaged_groups.len() - 1 {
                    continue;
                }

                let mut pattern = pattern.clone();

                pattern.append(gi, *pattern_index_first);

                for (j, pattern_index_second) in possible_indexes[gi + 1].iter().enumerate() {
                    if *pattern_index_second > *pattern_index_first + self.damaged_groups[gi]
                        && pattern
                            .can_be_appended(gi + 1, *pattern_index_second)
                            .is_some()
                    {
                        next[gi][i].insert(j);
                    }
                }
            }
        }
    }

    fn possible_indexes(&self) -> Vec<Vec<usize>> {
        let pattern =
            PatternCandidate::new(self.spring_conditions.clone(), self.damaged_groups.clone());

        let mut possible_indexes = vec![Vec::new(); self.damaged_groups.len()];

        for group_index in 0..self.damaged_groups.len() {
            let min_limit = pattern.min_limit(group_index);
            let max_limit = pattern.max_limit(group_index);

            for pattern_index in min_limit..max_limit {
                match pattern.can_be_appended(group_index, pattern_index) {
                    Some(_new_pattern) => {
                        possible_indexes[group_index].push(pattern_index);
                    }
                    None => {}
                }
            }
        }

        possible_indexes
    }

    pub fn parse(line: &String) -> anyhow::Result<Self> {
        let (spring_conditions, damaged_groups) = match line.split_once(" ") {
            Some((prefix, suffix)) => (prefix, suffix),
            None => bail!("Wrong input data"),
        };

        let spring_conditions = spring_conditions.try_into()?;

        let damaged_groups = damaged_groups
            .split(",")
            .map(|count| count.parse().context("Failed to parse group count"))
            .collect::<anyhow::Result<_>>()?;

        Ok(Self {
            spring_conditions,
            damaged_groups,
        })
    }

    pub fn parse2(line: &String) -> anyhow::Result<Self> {
        let (spring_conditions, damaged_groups) = match line.split_once(" ") {
            Some((prefix, suffix)) => (prefix, suffix),
            None => bail!("Wrong input data"),
        };

        let spring_conditions = vec![spring_conditions.to_owned(); 5];

        let spring_conditions = spring_conditions.join("?");

        let spring_conditions = spring_conditions.as_str().try_into()?;

        let damaged_groups = vec![damaged_groups.to_owned(); 5];

        let damaged_groups = damaged_groups.join(",");

        let damaged_groups = damaged_groups
            .split(",")
            .map(|count| count.parse().context("Failed to parse group count"))
            .collect::<anyhow::Result<_>>()?;

        Ok(Self {
            spring_conditions,
            damaged_groups,
        })
    }
}
