use anyhow::bail;
use std::collections::VecDeque;

type Coordinates = (usize, usize);

#[derive(Debug, Copy, Clone)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn shift(&self, coords: Coordinates) -> Coordinates {
        match self {
            Direction::North => (coords.0 - 1, coords.1),
            Direction::East => (coords.0, coords.1 + 1),
            Direction::South => (coords.0 + 1, coords.1),
            Direction::West => (coords.0, coords.1 - 1),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
enum Pipe {
    Vertical,
    Horizontal,
    NorthEastBend,
    NorthWestBend,
    SouthWestBend,
    SouthEastBend,
    Ground,
    Start,
}

impl Pipe {
    fn get_next(&self, neighbour: &Self, neighbour_direction: Direction) -> Option<Direction> {
        match (self, neighbour_direction, neighbour) {
            (
                Pipe::Vertical | Pipe::NorthEastBend | Pipe::NorthWestBend | Pipe::Start,
                Direction::North,
                Pipe::Vertical,
            ) => Some(Direction::North),
            (
                Pipe::Vertical | Pipe::NorthEastBend | Pipe::NorthWestBend | Pipe::Start,
                Direction::North,
                Pipe::SouthWestBend,
            ) => Some(Direction::West),
            (
                Pipe::Vertical | Pipe::NorthEastBend | Pipe::NorthWestBend | Pipe::Start,
                Direction::North,
                Pipe::SouthEastBend,
            ) => Some(Direction::East),
            (
                Pipe::Vertical | Pipe::SouthWestBend | Pipe::SouthEastBend | Pipe::Start,
                Direction::South,
                Pipe::Vertical,
            ) => Some(Direction::South),
            (
                Pipe::Vertical | Pipe::SouthWestBend | Pipe::SouthEastBend | Pipe::Start,
                Direction::South,
                Pipe::NorthEastBend,
            ) => Some(Direction::East),
            (
                Pipe::Vertical | Pipe::SouthWestBend | Pipe::SouthEastBend | Pipe::Start,
                Direction::South,
                Pipe::NorthWestBend,
            ) => Some(Direction::West),
            (
                Pipe::Horizontal | Pipe::NorthEastBend | Pipe::SouthEastBend | Pipe::Start,
                Direction::East,
                Pipe::Horizontal,
            ) => Some(Direction::East),
            (
                Pipe::Horizontal | Pipe::NorthEastBend | Pipe::SouthEastBend | Pipe::Start,
                Direction::East,
                Pipe::NorthWestBend,
            ) => Some(Direction::North),
            (
                Pipe::Horizontal | Pipe::NorthEastBend | Pipe::SouthEastBend | Pipe::Start,
                Direction::East,
                Pipe::SouthWestBend,
            ) => Some(Direction::South),
            (
                Pipe::Horizontal | Pipe::SouthWestBend | Pipe::NorthWestBend | Pipe::Start,
                Direction::West,
                Pipe::Horizontal,
            ) => Some(Direction::West),
            (
                Pipe::Horizontal | Pipe::SouthWestBend | Pipe::NorthWestBend | Pipe::Start,
                Direction::West,
                Pipe::NorthEastBend,
            ) => Some(Direction::North),
            (
                Pipe::Horizontal | Pipe::SouthWestBend | Pipe::NorthWestBend | Pipe::Start,
                Direction::West,
                Pipe::SouthEastBend,
            ) => Some(Direction::South),
            (_, _, _) => None,
        }
    }

    fn parse(symbol: char) -> anyhow::Result<Self> {
        let pipe = match symbol {
            '|' => Self::Vertical,
            '-' => Self::Horizontal,
            'L' => Self::NorthEastBend,
            'J' => Self::NorthWestBend,
            '7' => Self::SouthWestBend,
            'F' => Self::SouthEastBend,
            '.' => Self::Ground,
            'S' => Self::Start,
            _ => bail!("Wrong input"),
        };

        Ok(pipe)
    }
}

pub struct LoopMap {
    map: Vec<Vec<bool>>,
}

impl LoopMap {
    fn new(rows: usize, cols: usize) -> Self {
        let map = vec![vec![false; cols * 3]; rows * 3];

        Self { map }
    }

    fn add_pipe(&mut self, pipe: Pipe, coords: Coordinates) {
        self.map[coords.0 * 3 + 1][coords.1 * 3 + 1] = true;

        if pipe == Pipe::Vertical
            || pipe == Pipe::NorthEastBend
            || pipe == Pipe::NorthWestBend
            || pipe == Pipe::Start
        {
            self.map[coords.0 * 3][coords.1 * 3 + 1] = true;
        }

        if pipe == Pipe::Vertical
            || pipe == Pipe::SouthEastBend
            || pipe == Pipe::SouthWestBend
            || pipe == Pipe::Start
        {
            self.map[coords.0 * 3 + 2][coords.1 * 3 + 1] = true;
        }

        if pipe == Pipe::Horizontal
            || pipe == Pipe::SouthWestBend
            || pipe == Pipe::NorthWestBend
            || pipe == Pipe::Start
        {
            self.map[coords.0 * 3 + 1][coords.1 * 3] = true;
        }

        if pipe == Pipe::Horizontal
            || pipe == Pipe::SouthEastBend
            || pipe == Pipe::NorthEastBend
            || pipe == Pipe::Start
        {
            self.map[coords.0 * 3 + 1][coords.1 * 3 + 2] = true;
        }
    }

    pub fn flood(&mut self) {
        let mut stack = VecDeque::new();

        stack.push_back((0, 0));
        self.map[0][0] = true;

        while let Some((row, col)) = stack.pop_front() {
            if row > 0 {
                if !self.map[row - 1][col] {
                    self.map[row - 1][col] = true;
                    stack.push_back((row - 1, col));
                }
            }

            if row < self.map.len() - 1 {
                if !self.map[row + 1][col] {
                    self.map[row + 1][col] = true;
                    stack.push_back((row + 1, col));
                }
            }

            if col > 0 {
                if !self.map[row][col - 1] {
                    self.map[row][col - 1] = true;
                    stack.push_back((row, col - 1));
                }
            }

            if col < self.map[0].len() - 1 {
                if !self.map[row][col + 1] {
                    self.map[row][col + 1] = true;
                    stack.push_back((row, col + 1));
                }
            }
        }
    }

    pub fn count_enclosed(self) -> usize {
        self.map
            .into_iter()
            .enumerate()
            .filter(|(row_index, _)| row_index % 3 == 1)
            .flat_map(|(_, row)| {
                row.into_iter()
                    .enumerate()
                    .filter(|(col_index, col)| col_index % 3 == 1 && !*col)
            })
            .count()
    }
}

#[derive(Debug)]
pub struct Area {
    pipes: Vec<Vec<Pipe>>,
    start: Coordinates,
}

impl Area {
    pub fn get_pipes_loop_max_distance(&self) -> u64 {
        let mut step = 0;
        let mut coords = self.start;
        let mut pipe = Pipe::Start;
        let mut direction = Direction::North;

        for neighbour_direction in self.get_neighbours(coords) {
            let neighbour_coords = neighbour_direction.shift(coords);
            let neighbour = self.get_pipe(neighbour_coords);
            if let Some(next_direction) = pipe.get_next(&neighbour, neighbour_direction) {
                step += 1;
                coords = neighbour_coords;
                pipe = neighbour;
                direction = next_direction;

                break;
            }
        }

        loop {
            step += 1;
            coords = direction.shift(coords);
            let next_pipe = self.get_pipe(coords);

            if next_pipe == Pipe::Start {
                break;
            }

            direction = pipe.get_next(&next_pipe, direction).unwrap();
            pipe = next_pipe;
        }

        step / 2
    }

    pub fn get_loop_map(&self) -> LoopMap {
        let mut coords = self.start;
        let mut pipe = Pipe::Start;
        let mut direction = Direction::North;

        let mut loop_map = LoopMap::new(self.pipes.len(), self.pipes[0].len());

        for neighbour_direction in self.get_neighbours(coords) {
            let neighbour_coords = neighbour_direction.shift(coords);
            let neighbour = self.get_pipe(neighbour_coords);
            if let Some(next_direction) = pipe.get_next(&neighbour, neighbour_direction) {
                coords = neighbour_coords;
                pipe = neighbour;
                direction = next_direction;

                loop_map.add_pipe(pipe.clone(), coords);

                break;
            }
        }

        loop {
            coords = direction.shift(coords);
            let next_pipe = self.get_pipe(coords);

            loop_map.add_pipe(next_pipe.clone(), coords);

            if next_pipe == Pipe::Start {
                break;
            }

            direction = pipe.get_next(&next_pipe, direction).unwrap();
            pipe = next_pipe;
        }

        loop_map
    }

    fn get_neighbours(&self, coords: Coordinates) -> Vec<Direction> {
        let mut neighbours = Vec::new();

        if coords.0 >= self.pipes.len() || coords.1 >= self.pipes[0].len() {
            return neighbours;
        }

        if coords.0 != 0 {
            neighbours.push(Direction::North);
        }

        if coords.1 != 0 {
            neighbours.push(Direction::West);
        }

        if coords.0 < self.pipes.len() - 1 {
            neighbours.push(Direction::South);
        }

        if coords.1 < self.pipes[0].len() - 1 {
            neighbours.push(Direction::East);
        }

        neighbours
    }

    fn get_pipe(&self, coords: Coordinates) -> Pipe {
        self.pipes[coords.0][coords.1].clone()
    }

    pub fn parse(lines: Vec<String>) -> anyhow::Result<Self> {
        if lines.is_empty() {
            bail!("Empty input data")
        }

        let mut pipes = vec![Vec::new(); lines.len()];

        let mut start = (0, 0);

        for (row_index, line) in lines.into_iter().enumerate() {
            for (col_index, symbol) in line.chars().enumerate() {
                if symbol == 'S' {
                    start = (row_index, col_index);
                }

                let pipe = Pipe::parse(symbol)?;

                pipes[row_index].push(pipe);
            }
        }

        Ok(Self { pipes, start })
    }
}
