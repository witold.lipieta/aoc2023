use anyhow::{Context, Result};
use clap::Parser;
use std::path::PathBuf;

pub mod d01;
pub mod d02;
pub mod d03;
pub mod d04;
pub mod d05;
pub mod d06;
pub mod d07;
pub mod d08;
pub mod d09;
pub mod d10;
pub mod d11;
pub mod d12;
pub mod d13;
pub mod d14;
pub mod d15;
pub mod d16;
pub mod d19;
pub mod d22;
pub mod d23;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Path to file with input data
    #[arg(short, long)]
    input: PathBuf,
}

pub async fn read_input_data() -> Result<String> {
    let args = Args::parse();

    tokio::fs::read_to_string(args.input)
        .await
        .context("Couldn't read input file")
}

pub async fn input_as_lines() -> Result<Vec<String>> {
    let input_data = read_input_data().await?;

    Ok(input_data.lines().map(|l| l.to_string()).collect())
}
