use anyhow::{anyhow, bail, Context, Result};
use std::cmp::Ordering;
use std::collections::HashMap;

#[derive(Hash, Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
enum CamelCard {
    Joker,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

impl CamelCard {
    fn parse(value: char, with_jokers: bool) -> Result<Self> {
        let camel_card = match value {
            '2' => Self::Two,
            '3' => Self::Three,
            '4' => Self::Four,
            '5' => Self::Five,
            '6' => Self::Six,
            '7' => Self::Seven,
            '8' => Self::Eight,
            '9' => Self::Nine,
            'T' => Self::Ten,
            'J' if with_jokers => Self::Joker,
            'J' => Self::Jack,
            'Q' => Self::Queen,
            'K' => Self::King,
            'A' => Self::Ace,
            _ => bail!("Wrong card character"),
        };

        Ok(camel_card)
    }
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfKind,
    FullHouse,
    FourOfKind,
    FiveOfKind,
}

#[derive(Clone, Debug, Ord, Eq)]
struct Hand {
    cards: [CamelCard; 5],
    bid: usize,
}

impl Hand {
    fn get_type(&self) -> HandType {
        let mut card_counts = HashMap::new();
        for card in self.cards {
            let count = card_counts.get(&card).get_or_insert(&0usize).clone();
            card_counts.insert(card, count + 1);
        }

        let jokers = card_counts
            .remove(&CamelCard::Joker)
            .get_or_insert(0)
            .clone();

        let mut counts = card_counts
            .values()
            .map(|count| *count)
            .collect::<Vec<usize>>();

        counts.sort();

        let max = counts.iter().max().get_or_insert(&0).clone() + jokers;

        if max == 5 {
            HandType::FiveOfKind
        } else if max == 4 {
            HandType::FourOfKind
        } else if counts.len() == 2 {
            HandType::FullHouse
        } else if max == 3 {
            HandType::ThreeOfKind
        } else if counts.len() == 3 {
            HandType::TwoPair
        } else if max == 2 {
            HandType::OnePair
        } else {
            HandType::HighCard
        }
    }

    fn parse(value: String, with_jokers: bool) -> Result<Self> {
        if value.len() < 7 {
            bail!("Wrong input data")
        }

        let bid = value[6..].parse().context("Wrong bid input format")?;

        let cards = value[..5]
            .chars()
            .map(|card| CamelCard::parse(card, with_jokers))
            .collect::<Result<Vec<_>>>()
            .context("Wrong hand input format")?
            .try_into()
            .map_err(|_| anyhow!("CamelCard Hand conversion error"))?;

        Ok(Self { cards, bid })
    }
}

impl PartialEq<Self> for Hand {
    fn eq(&self, other: &Self) -> bool {
        self.cards == other.cards
    }
}

impl PartialOrd<Self> for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self.get_type().cmp(&other.get_type()) {
            Ordering::Equal => {
                let mut ord = Ordering::Equal;
                for index in 0..5 {
                    ord = self.cards[index].cmp(&other.cards[index]);

                    if ord != Ordering::Equal {
                        break;
                    }
                }

                Some(ord)
            }
            ord => Some(ord),
        }
    }
}

#[derive(Debug)]
pub struct SetOfHands {
    hands: Vec<Hand>,
}

impl SetOfHands {
    pub fn score(&self) -> usize {
        let mut score = 0;

        let mut hands = self.hands.clone();

        hands.sort();

        for (i, e) in hands.iter().enumerate() {
            score += (i + 1) * e.bid;
        }

        score
    }

    pub fn parse(lines: Vec<String>, with_jokers: bool) -> Result<Self> {
        let hands = lines
            .into_iter()
            .map(|line| Hand::parse(line, with_jokers))
            .collect::<Result<_>>()
            .context("Wrong input")?;

        Ok(Self { hands })
    }
}
