# AOC2023

Solutions for Advent of Code 2023

## Getting started

Run example for given day:

```
cargo run --package aoc2023 --example d01e1 -- --input input/td01e1
```
