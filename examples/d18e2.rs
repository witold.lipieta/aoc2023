use anyhow::{bail, Context, Error, Result};
use aoc2023::input_as_lines;
use std::collections::BTreeSet;
use std::fmt::{Display, Formatter};
use std::ops::Range;

type RelativeCoordinates = (i64, i64);

#[derive(Debug, Copy, Clone, Eq, Hash, PartialEq)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl TryFrom<&str> for Direction {
    type Error = Error;

    fn try_from(value: &str) -> std::result::Result<Self, Self::Error> {
        let direction = match value {
            "U" => Self::Up,
            "R" => Self::Right,
            "D" => Self::Down,
            "L" => Self::Left,
            _ => bail!("Wrong direction str"),
        };

        Ok(direction)
    }
}

impl TryFrom<char> for Direction {
    type Error = Error;

    fn try_from(value: char) -> std::result::Result<Self, Self::Error> {
        let direction = match value {
            '3' => Self::Up,
            '0' => Self::Right,
            '1' => Self::Down,
            '2' => Self::Left,
            _ => bail!("Wrong direction char"),
        };

        Ok(direction)
    }
}

#[derive(Debug)]
struct TrenchPlan {
    row_ranges: Vec<Range<i64>>,
    vertical_edges: Vec<(i64, Range<i64>)>,
}

impl TrenchPlan {
    fn count_area(&self) -> usize {
        let mut count = 0;
        for row_range in &self.row_ranges {
            let mut vertical_edges = self
                .vertical_edges
                .iter()
                .filter(|ve| ve.1.contains(&row_range.start))
                .map(|ve| ve.clone())
                .collect::<Vec<_>>();

            vertical_edges.sort_by_key(|ve| ve.0);

            for i in 0..vertical_edges.len() / 2 {
                count += (vertical_edges[i * 2].0..vertical_edges[i * 2 + 1].0 + 1)
                    .into_iter()
                    .count()
                    * row_range.clone().into_iter().count();
            }
        }

        count
    }

    fn count_next(
        current: RelativeCoordinates,
        direction: Direction,
        count: usize,
    ) -> RelativeCoordinates {
        match direction {
            Direction::Up => (current.0 + count as i64, current.1),
            Direction::Right => (current.0, current.1 + count as i64),
            Direction::Down => (current.0 - count as i64, current.1),
            Direction::Left => (current.0, current.1 - count as i64),
        }
    }
}

impl From<DigPlan> for TrenchPlan {
    fn from(dig_plan: DigPlan) -> Self {
        let mut current = (0, 0);

        let mut nodes = Vec::new();

        let mut rows = BTreeSet::new();

        for (direction, count) in &dig_plan.trenches {
            let next = Self::count_next(current, *direction, *count);

            rows.insert(next.0);

            current = next;
            nodes.push(current);
        }

        let mut vertical_edges = Vec::new();

        for i in 0..nodes.len() {
            let prev_i = if i == 0 { nodes.len() - 1 } else { i - 1 };
            let next_i = if i == nodes.len() - 1 { 0 } else { i + 1 };

            let dir = dig_plan.trenches[i].0;
            let prev_dir = dig_plan.trenches[prev_i].0;
            let next_dir = dig_plan.trenches[next_i].0;

            if dir == Direction::Down {
                let mut ver = nodes[i].0 + 1..nodes[prev_i].0;

                if prev_dir == Direction::Right {
                    ver.end = nodes[prev_i].0 + 1;
                }

                if next_dir == Direction::Left {
                    ver.start = nodes[i].0;
                }

                vertical_edges.push((nodes[i].1, ver));
            }

            if dir == Direction::Up {
                let mut ver = nodes[prev_i].0 + 1..nodes[i].0;

                if next_dir == Direction::Right {
                    ver.end = nodes[i].0 + 1;
                }

                if prev_dir == Direction::Left {
                    ver.start = nodes[prev_i].0;
                }

                vertical_edges.push((nodes[i].1, ver));
            }
        }

        let mut row_ranges = Vec::new();

        let rows = rows.into_iter().collect::<Vec<_>>();

        for row_i in 0..rows.len() {
            let row_range = rows[row_i]..rows[row_i] + 1;

            row_ranges.push(row_range);

            if row_i < rows.len() - 1 {
                let between_range = rows[row_i] + 1..rows[row_i + 1];

                row_ranges.push(between_range);
            }
        }

        Self {
            row_ranges,
            vertical_edges,
        }
    }
}

impl Display for TrenchPlan {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "TrenchPlan:\n")?;

        let min_row = self.row_ranges[0].start;
        let max_row = self.row_ranges[self.row_ranges.len() - 1].end;

        let min_col = self.vertical_edges.iter().min_by_key(|ve| ve.0).unwrap().0;
        let max_col = self.vertical_edges.iter().max_by_key(|ve| ve.0).unwrap().0;

        let line_len = (min_col..max_col + 1).into_iter().count();

        let range_starts = self.row_ranges.iter().map(|r| r.start).collect::<Vec<_>>();

        for row in (min_row..max_row).into_iter().rev() {
            if range_starts.contains(&row) {
                write!(f, "@")?;
            } else {
                write!(f, ".")?;
            }
            let mut line = vec!['.'; line_len];
            for ve in self
                .vertical_edges
                .iter()
                .filter(|ve| ve.1.contains(&row))
                .map(|ve| ve.0)
            {
                line[(ve - min_col) as usize] = '#';
            }

            for symbol in line {
                write!(f, "{}", symbol)?;
            }

            write!(f, "\n")?;
        }

        Ok(())
    }
}

#[derive(Debug)]
struct DigPlan {
    trenches: Vec<(Direction, usize)>,
}

impl DigPlan {
    #[allow(unused)]
    fn parse(lines: Vec<String>) -> Result<Self> {
        let mut trenches = Vec::new();

        for line in lines {
            let trench = line.split(" ").collect::<Vec<_>>();

            if trench.len() != 3 {
                bail!("Wrong trench input data")
            }

            let direction = trench[0].try_into().context("Failed to parse direction")?;
            let count = trench[1].parse().context("Failed to parse count")?;

            trenches.push((direction, count));
        }

        Ok(Self { trenches })
    }

    #[allow(unused)]
    fn parse2(lines: Vec<String>) -> Result<Self> {
        let mut trenches = Vec::new();

        for line in lines {
            let trench = line.split(" ").collect::<Vec<_>>();

            if trench.len() != 3 {
                bail!("Wrong trench input data")
            }

            let digits = trench[2]
                .chars()
                .skip(2)
                .take(5)
                .map(|d| d.to_digit(16).unwrap())
                .collect::<Vec<_>>();

            let mut count: usize = 0;

            for i in 0..5 {
                count += (digits[i] * 16_u32.pow(5 - i as u32 - 1)) as usize;
            }

            let direction = trench[2]
                .chars()
                .skip(7)
                .next()
                .unwrap()
                .try_into()
                .context("Failed to parse direction")?;

            trenches.push((direction, count));
        }

        Ok(Self { trenches })
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let dig_plan = DigPlan::parse2(lines)?;

    let trench_plan = TrenchPlan::from(dig_plan);

    // println!("{}", trench_plan);
    println!("{}", trench_plan.count_area());

    Ok(())
}
