use anyhow::Result;
use aoc2023::d13::Pattern;
use aoc2023::read_input_data;

#[tokio::main]
async fn main() -> Result<()> {
    let input_data = read_input_data().await?;

    let patterns = input_data.split("\n\n").collect::<Vec<_>>();

    let sum = patterns
        .into_iter()
        .map(|pattern| Pattern::parse(pattern).and_then(|pattern| Ok(pattern.score2())))
        .collect::<Result<Vec<_>>>()?
        .iter()
        .sum::<usize>();

    println!("{:?}", sum);

    Ok(())
}
