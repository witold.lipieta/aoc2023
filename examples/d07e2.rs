use anyhow::Result;
use aoc2023::d07::SetOfHands;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let set_of_hands = SetOfHands::parse(lines, true)?;

    println!("{:?}", set_of_hands.score());

    Ok(())
}
