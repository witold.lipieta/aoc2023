use anyhow::{bail, Result};
use aoc2023::input_as_lines;
use std::collections::{HashMap, VecDeque};
use std::fmt::{Display, Formatter};

type Coordinates = (usize, usize);

#[derive(Copy, Clone)]
enum NeighbourGarden {
    Top,
    Bottom,
    Left,
    Right,
}

enum FloodStart {
    Point(Coordinates),
    Top,
    Bottom,
    Left,
    Right,
}

#[derive(Debug)]
struct Garden {
    obstacles: Vec<Vec<bool>>,
    steps: Vec<Vec<usize>>,
}

impl Garden {
    fn neighbour(&self, neighbour_garden: NeighbourGarden) -> (Self, FloodStart) {
        let rows = self.obstacles.len();
        let cols = self.obstacles[0].len();
        let mut steps = vec![vec![usize::MAX; cols]; rows];

        let start_flood = match neighbour_garden {
            NeighbourGarden::Top => {
                for (col, s) in self.steps[0].iter().enumerate() {
                    if *s < usize::MAX {
                        steps[rows - 1][col] = *s + 1;
                    }
                }

                FloodStart::Bottom
            }
            NeighbourGarden::Bottom => {
                for (col, s) in self.steps[rows - 1].iter().enumerate() {
                    if *s < usize::MAX {
                        steps[0][col] = *s + 1;
                    }
                }

                FloodStart::Top
            }
            NeighbourGarden::Left => {
                for row in 0..rows {
                    let s = self.steps[row][0];

                    if s < usize::MAX {
                        steps[row][cols - 1] = s + 1;
                    }
                }

                FloodStart::Right
            }
            NeighbourGarden::Right => {
                for row in 0..rows {
                    let s = self.steps[row][cols - 1];

                    if s < usize::MAX {
                        steps[row][0] = self.steps[row][cols - 1] + 1;
                    }
                }

                FloodStart::Left
            }
        };

        (
            Self {
                obstacles: self.obstacles.clone(),
                steps,
            },
            start_flood,
        )
    }

    fn score(&self, steps: usize) -> usize {
        self.steps
            .iter()
            .flatten()
            .filter(|s| **s <= steps && 0 == **s % 2)
            .count()
    }

    fn flood(&mut self, flood_start: &FloodStart) {
        let rows = self.obstacles.len();
        let cols = self.obstacles[0].len();
        let mut stack = VecDeque::new();

        match flood_start {
            FloodStart::Point(start) => {
                stack.push_back((start.0, start.1));
            }
            FloodStart::Top => {
                for col in 0..cols {
                    stack.push_back((0, col));
                }
            }
            FloodStart::Bottom => {
                for col in 0..cols {
                    stack.push_back((rows - 1, col));
                }
            }
            FloodStart::Left => {
                for row in 0..rows {
                    stack.push_back((row, 0));
                }
            }
            FloodStart::Right => {
                for row in 0..rows {
                    stack.push_back((row, rows - 1));
                }
            }
        }

        while let Some((r, c)) = stack.pop_front() {
            let next_steps = self.steps[r][c] + 1;

            if r + 1 < rows {
                if !self.obstacles[r + 1][c] {
                    if self.steps[r + 1][c] > next_steps {
                        self.steps[r + 1][c] = next_steps;
                        stack.push_back((r + 1, c));
                    }
                }
            }

            if c + 1 < cols {
                if !self.obstacles[r][c + 1] {
                    if self.steps[r][c + 1] > next_steps {
                        self.steps[r][c + 1] = next_steps;
                        stack.push_back((r, c + 1));
                    }
                }
            }

            if r > 0 {
                if !self.obstacles[r - 1][c] {
                    if self.steps[r - 1][c] > next_steps {
                        self.steps[r - 1][c] = next_steps;
                        stack.push_back((r - 1, c));
                    }
                }
            }

            if c > 0 {
                if !self.obstacles[r][c - 1] {
                    if self.steps[r][c - 1] > next_steps {
                        self.steps[r][c - 1] = next_steps;
                        stack.push_back((r, c - 1));
                    }
                }
            }
        }
    }

    fn parse(input: Vec<String>) -> Result<(Self, Coordinates)> {
        let mut start = (0, 0);

        if input.is_empty() {
            bail!("Wrong input");
        }

        let rows = input.len();
        let cols = input[0].len();

        let mut obstacles = vec![vec![false; cols]; rows];
        let mut steps = vec![vec![usize::MAX; cols]; rows];

        for (row, line) in input.into_iter().enumerate() {
            for (col, symbol) in line.chars().enumerate() {
                match symbol {
                    'S' => {
                        start = (row, col);
                        steps[row][col] = 0;
                    }
                    '#' => {
                        obstacles[row][col] = true;
                    }
                    _ => {}
                }
            }
        }

        Ok((Self { obstacles, steps }, start))
    }
}

impl Display for Garden {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let rows = self.obstacles.len();
        let cols = self.obstacles[0].len();

        for row in 0..rows {
            for col in 0..cols {
                if self.obstacles[row][col] {
                    write!(f, "#####")?;
                } else if self.steps[row][col] == usize::MAX {
                    write!(f, "     ")?;
                } else {
                    write!(f, "{:4} ", self.steps[row][col])?;
                }
            }
            write!(f, "\n")?;
        }
        write!(f, "\n")?;

        Ok(())
    }
}

struct InfiniteGardenPart {
    position: (i64, i64),
    flood_start: FloodStart,
    garden: Garden,
}

impl InfiniteGardenPart {
    fn neighbour(&self, neighbour_garden: NeighbourGarden) -> Self {
        let (garden, flood_start) = self.garden.neighbour(neighbour_garden);

        let position = match neighbour_garden {
            NeighbourGarden::Top => (self.position.0 - 1, self.position.1),
            NeighbourGarden::Bottom => (self.position.0 + 1, self.position.1),
            NeighbourGarden::Left => (self.position.0, self.position.1 - 1),
            NeighbourGarden::Right => (self.position.0, self.position.1 + 1),
        };

        Self::new(position, flood_start, garden)
    }

    fn score(&mut self, max_steps: usize) -> usize {
        self.garden.score(max_steps)
    }

    fn flood(&mut self) {
        self.garden.flood(&self.flood_start);
    }

    pub fn new(position: (i64, i64), flood_start: FloodStart, garden: Garden) -> Self {
        Self {
            position,
            flood_start,
            garden,
        }
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let (garden, start) = Garden::parse(lines)?;

    let mut starting_garden = InfiniteGardenPart::new((0, 0), FloodStart::Point(start), garden);

    starting_garden.flood();

    println!("{}", starting_garden.score(64));

    Ok(())
}

fn part2(max_steps: usize, starting_garden: InfiniteGardenPart) -> HashMap<(i64, i64), usize> {
    let mut calculated_gardens: HashMap<(i64, i64), usize> = HashMap::new();

    let mut stack = VecDeque::new();

    stack.push_back(starting_garden);

    while let Some(mut garden) = stack.pop_front() {
        // println!("flooding garden {:?}", garden.position);
        garden.flood();

        let garden_score = garden.score(max_steps);

        if garden_score == 0 {
            continue;
        }

        let current_score = calculated_gardens
            .get(&garden.position)
            .cloned()
            .unwrap_or(0);

        if current_score < garden_score {
            // println!("better score {:?} -> {}", garden.position, garden_score);
            // println!("{}", garden.garden);
            calculated_gardens.insert(garden.position, garden_score);

            match garden.flood_start {
                FloodStart::Point(_) => {
                    stack.push_back(garden.neighbour(NeighbourGarden::Top));
                    stack.push_back(garden.neighbour(NeighbourGarden::Bottom));
                    stack.push_back(garden.neighbour(NeighbourGarden::Left));
                    stack.push_back(garden.neighbour(NeighbourGarden::Right));
                }
                FloodStart::Top => {
                    stack.push_back(garden.neighbour(NeighbourGarden::Top));
                    stack.push_back(garden.neighbour(NeighbourGarden::Right));
                    stack.push_back(garden.neighbour(NeighbourGarden::Left));
                }
                FloodStart::Bottom => {
                    stack.push_back(garden.neighbour(NeighbourGarden::Bottom));
                    stack.push_back(garden.neighbour(NeighbourGarden::Right));
                    stack.push_back(garden.neighbour(NeighbourGarden::Left));
                }
                FloodStart::Left => {
                    stack.push_back(garden.neighbour(NeighbourGarden::Top));
                    stack.push_back(garden.neighbour(NeighbourGarden::Bottom));
                    stack.push_back(garden.neighbour(NeighbourGarden::Left));
                }
                FloodStart::Right => {
                    stack.push_back(garden.neighbour(NeighbourGarden::Top));
                    stack.push_back(garden.neighbour(NeighbourGarden::Bottom));
                    stack.push_back(garden.neighbour(NeighbourGarden::Right));
                }
            }
        }
    }
    calculated_gardens
}
