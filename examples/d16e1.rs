use anyhow::Result;
use aoc2023::d16::{Direction, Energizer};
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let mut energizer = Energizer::parse(lines)?;

    energizer.cast_beam((0, 0), Direction::East);

    println!("{:?}", energizer.score());

    Ok(())
}
