use anyhow::{bail, Context, Error, Result};
use aoc2023::input_as_lines;
use std::cmp::{max, min};
use std::collections::VecDeque;
use std::fmt::{Display, Formatter};

type RelativeCoordinates = (i64, i64);

#[derive(Debug, Copy, Clone, Eq, Hash, PartialEq)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl TryFrom<&str> for Direction {
    type Error = Error;

    fn try_from(value: &str) -> std::result::Result<Self, Self::Error> {
        let direction = match value {
            "U" => Self::Up,
            "R" => Self::Right,
            "D" => Self::Down,
            "L" => Self::Left,
            _ => bail!("Wrong direction str"),
        };

        Ok(direction)
    }
}

impl TryFrom<char> for Direction {
    type Error = Error;

    fn try_from(value: char) -> std::result::Result<Self, Self::Error> {
        let direction = match value {
            '3' => Self::Up,
            '0' => Self::Right,
            '1' => Self::Down,
            '2' => Self::Left,
            _ => bail!("Wrong direction char"),
        };

        Ok(direction)
    }
}

#[derive(Clone, PartialEq)]
enum Trench {
    Ground,
    Edge,
    Pit,
}

struct TrenchPlan {
    trench_map: Vec<Vec<Trench>>,
}

impl TrenchPlan {
    fn score(&self) -> usize {
        let mut count = 0;
        for row in &self.trench_map {
            for trench in row {
                if *trench != Trench::Ground {
                    count += 1;
                }
            }
        }

        count
    }

    fn get_next(
        current: RelativeCoordinates,
        direction: Direction,
        count: usize,
    ) -> Vec<RelativeCoordinates> {
        (1..count + 1)
            .into_iter()
            .map(|count| match direction {
                Direction::Up => (current.0 + count as i64, current.1),
                Direction::Right => (current.0, current.1 + count as i64),
                Direction::Down => (current.0 - count as i64, current.1),
                Direction::Left => (current.0, current.1 - count as i64),
            })
            .collect()
    }
}

impl From<DigPlan> for TrenchPlan {
    fn from(dig_plan: DigPlan) -> Self {
        let mut current = (0, 0);

        let mut trench_loop = Vec::from([current]);

        let mut max_coords = (0, 0);
        let mut min_coords = (0, 0);

        for (direction, count) in dig_plan.trenches {
            let mut next = Self::get_next(current, direction, count);

            current = next.last().copied().unwrap();

            max_coords.0 = max(max_coords.0, current.0);
            max_coords.1 = max(max_coords.1, current.1);
            min_coords.0 = min(min_coords.0, current.0);
            min_coords.1 = min(min_coords.1, current.1);

            trench_loop.append(&mut next);
        }

        let offset = (0 - min_coords.0 + 1, 0 - min_coords.1 + 1);

        let max_coords = (
            (max_coords.0 + offset.0) as usize,
            (max_coords.1 + offset.1) as usize,
        );

        let trench_loop = trench_loop
            .iter()
            .map(|coords| {
                (
                    (coords.0 + offset.0) as usize,
                    (coords.1 + offset.1) as usize,
                )
            })
            .collect::<Vec<_>>();

        let mut trench_map = vec![vec![Trench::Pit; max_coords.1 + 2]; max_coords.0 + 2];

        for coords in trench_loop {
            trench_map[coords.0][coords.1] = Trench::Edge;
        }

        let mut stack = VecDeque::new();

        stack.push_back((0, 0));
        trench_map[0][0] = Trench::Ground;

        while let Some((row, col)) = stack.pop_front() {
            if row > 0 {
                if trench_map[row - 1][col] == Trench::Pit {
                    trench_map[row - 1][col] = Trench::Ground;
                    stack.push_back((row - 1, col));
                }
            }

            if row < trench_map.len() - 1 {
                if trench_map[row + 1][col] == Trench::Pit {
                    trench_map[row + 1][col] = Trench::Ground;
                    stack.push_back((row + 1, col));
                }
            }

            if col > 0 {
                if trench_map[row][col - 1] == Trench::Pit {
                    trench_map[row][col - 1] = Trench::Ground;
                    stack.push_back((row, col - 1));
                }
            }

            if col < trench_map[0].len() - 1 {
                if trench_map[row][col + 1] == Trench::Pit {
                    trench_map[row][col + 1] = Trench::Ground;
                    stack.push_back((row, col + 1));
                }
            }
        }

        Self { trench_map }
    }
}

impl Display for TrenchPlan {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for row in &self.trench_map {
            for col in row {
                let symbol = match col {
                    Trench::Ground => '.',
                    Trench::Edge => '#',
                    Trench::Pit => '@',
                };

                write!(f, "{}", symbol)?;
            }

            write!(f, "\n")?;
        }

        Ok(())
    }
}

#[derive(Debug)]
struct DigPlan {
    trenches: Vec<(Direction, usize)>,
}

impl DigPlan {
    #[allow(unused)]
    fn parse(lines: Vec<String>) -> Result<Self> {
        let mut trenches = Vec::new();

        for line in lines {
            let trench = line.split(" ").collect::<Vec<_>>();

            if trench.len() != 3 {
                bail!("Wrong trench input data")
            }

            let direction = trench[0].try_into().context("Failed to parse direction")?;
            let count = trench[1].parse().context("Failed to parse count")?;

            trenches.push((direction, count));
        }

        Ok(Self { trenches })
    }

    #[allow(unused)]
    fn parse2(lines: Vec<String>) -> Result<Self> {
        let mut trenches = Vec::new();

        for line in lines {
            let trench = line.split(" ").collect::<Vec<_>>();

            if trench.len() != 3 {
                bail!("Wrong trench input data")
            }

            let digits = trench[2]
                .chars()
                .skip(2)
                .take(5)
                .map(|d| d.to_digit(16).unwrap())
                .collect::<Vec<_>>();

            let mut count: usize = 0;

            for i in 0..5 {
                count += (digits[i] * 16_u32.pow(5 - i as u32 - 1)) as usize;
            }

            let direction = trench[2]
                .chars()
                .skip(7)
                .next()
                .unwrap()
                .try_into()
                .context("Failed to parse direction")?;

            trenches.push((direction, count));
        }

        Ok(Self { trenches })
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let dig_plan = DigPlan::parse(lines)?;

    let trench_plan = TrenchPlan::from(dig_plan);

    println!("{}", trench_plan);
    println!("{}", trench_plan.score());

    Ok(())
}
