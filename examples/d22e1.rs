use anyhow::Result;
use aoc2023::d22::Snapshot;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let mut snapshot = Snapshot::parse(lines)?;

    println!("{}", snapshot.can_be_removed());

    Ok(())
}
