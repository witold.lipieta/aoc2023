use aoc2023::input_as_lines;

use anyhow::Result;

// run example with: cargo run --example read_input -- --input path/to/input
#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    println!("{:?}", lines);

    Ok(())
}
