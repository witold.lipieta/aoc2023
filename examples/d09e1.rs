use anyhow::Result;
use aoc2023::d09::Oasis;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let oasis = Oasis::parse(lines)?;

    println!("{:?}", oasis.score());

    Ok(())
}
