use anyhow::Result;
use aoc2023::d10::Area;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let area = Area::parse(lines)?;

    let mut map = area.get_loop_map();

    map.flood();

    println!("{:?}", map.count_enclosed());

    Ok(())
}
