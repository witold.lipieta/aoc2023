use anyhow::Result;
use aoc2023::d23::{Graph, Map};
use aoc2023::input_as_lines;
use std::fmt::Display;

fn part2(mut map: &mut Map) -> i64 {
    let start = map.starting_node();
    let goal = map.ending_node();

    let graph = Graph::read_map2(&mut map);

    let longest_path_distance = graph.calculate_longest_path2(start, goal) - 1;

    longest_path_distance
}

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let mut map = Map::parse(lines)?;

    let longest_path_distance = part2(&mut map);

    println!("{:?}", longest_path_distance);

    Ok(())
}
