use anyhow::Result;
use aoc2023::d05::Almanac;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let almanac = Almanac::parse2(lines)?;

    println!("{:?}", almanac.get_seeds_locations().min());

    Ok(())
}
