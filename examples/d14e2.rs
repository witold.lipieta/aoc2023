use anyhow::Result;
use aoc2023::d14::Platform;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let mut platform = Platform::parse(lines)?;

    platform.cycle(1000000000);

    println!("{:?}", platform.total_load());

    Ok(())
}
