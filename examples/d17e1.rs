use anyhow::{bail, Context, Result};
use aoc2023::input_as_lines;
use std::collections::HashSet;
use std::fmt::{Display, Formatter};

type Coordinates = (usize, usize);

#[derive(Debug)]
enum Side {
    Left,
    Right,
}

#[derive(Debug, Copy, Clone, Eq, Hash, PartialEq)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn index(&self) -> usize {
        match self {
            Direction::North => 0,
            Direction::East => 1,
            Direction::South => 2,
            Direction::West => 3,
        }
    }

    fn turn(&self, side: Side) -> Self {
        match (self, side) {
            (Direction::North, Side::Left) => Direction::West,
            (Direction::North, Side::Right) => Direction::East,
            (Direction::East, Side::Right) => Direction::South,
            (Direction::East, Side::Left) => Direction::North,
            (Direction::South, Side::Right) => Direction::West,
            (Direction::South, Side::Left) => Direction::East,
            (Direction::West, Side::Right) => Direction::North,
            (Direction::West, Side::Left) => Direction::South,
        }
    }

    fn shift(&self, coords: Coordinates, rows: usize, cols: usize) -> Option<Coordinates> {
        match self {
            Direction::North => {
                if coords.0 > 0 {
                    Some((coords.0 - 1, coords.1))
                } else {
                    None
                }
            }
            Direction::East => {
                if coords.1 < cols - 1 {
                    Some((coords.0, coords.1 + 1))
                } else {
                    None
                }
            }
            Direction::South => {
                if coords.0 < rows - 1 {
                    Some((coords.0 + 1, coords.1))
                } else {
                    None
                }
            }
            Direction::West => {
                if coords.1 > 0 {
                    Some((coords.0, coords.1 - 1))
                } else {
                    None
                }
            }
        }
    }
}

#[derive(Clone, Hash, Eq, PartialEq)]
struct State {
    coords: Coordinates,
    direction: Direction,
    forward_counter: usize,
}

impl State {
    pub fn new(coords: Coordinates, direction: Direction, forward_counter: usize) -> Self {
        Self {
            coords,
            direction,
            forward_counter,
        }
    }
}

#[allow(dead_code)]
enum Part {
    First,
    Second,
}

#[derive(Debug, Clone)]
pub struct HeatMap {
    rows: usize,
    cols: usize,
    map: Vec<Vec<usize>>,
    heat_loss: Vec<Vec<Vec<Vec<usize>>>>,
    f_score: Vec<Vec<Vec<Vec<usize>>>>,
    h_score: Vec<Vec<Vec<Vec<usize>>>>,
}

impl HeatMap {
    fn neighbours2(&self, current_state: &State) -> Vec<State> {
        let mut next_moves = Vec::new();
        if current_state.forward_counter < 9 {
            self.forward(current_state, &mut next_moves);
        }

        if current_state.forward_counter > 2 {
            self.turn(current_state, &mut next_moves, Side::Left);

            self.turn(current_state, &mut next_moves, Side::Right);
        }

        next_moves
    }

    fn neighbours(&self, current_state: &State) -> Vec<State> {
        let mut next_moves = Vec::new();

        if current_state.forward_counter < 2 {
            self.forward(current_state, &mut next_moves);
        }

        self.turn(current_state, &mut next_moves, Side::Left);

        self.turn(current_state, &mut next_moves, Side::Right);

        next_moves
    }

    fn turn(&self, current_state: &State, next_moves: &mut Vec<State>, side: Side) {
        let next_direction = current_state.direction.turn(side);

        if let Some(next_coords) = next_direction.shift(current_state.coords, self.rows, self.cols)
        {
            next_moves.push(State::new(next_coords, next_direction, 0));
        }
    }

    fn forward(&self, current_state: &State, next_moves: &mut Vec<State>) {
        if let Some(next_coords) =
            current_state
                .direction
                .shift(current_state.coords, self.rows, self.cols)
        {
            next_moves.push(State::new(
                next_coords,
                current_state.direction,
                current_state.forward_counter + 1,
            ));
        }
    }

    fn a_star(&mut self, part: Part) {
        let start = (0, 0);
        let goal = (self.rows - 1, self.cols - 1);

        let mut closed = HashSet::new();
        let mut open = HashSet::new();

        open.insert(State::new(start, Direction::East, 0));
        // open.insert(State::new(start, Direction::South, 0));

        while !open.is_empty() {
            let node = open
                .iter()
                .min_by(|node_a, node_b| self.get_f_score(node_a).cmp(&self.get_f_score(node_b)))
                .cloned()
                .unwrap();

            if node.coords == goal && node.forward_counter > 2 {
                return;
            }

            open.remove(&node);
            closed.insert(node.clone());

            let neighbours = match part {
                Part::First => self.neighbours(&node),
                Part::Second => self.neighbours2(&node),
            };

            for next_node in neighbours {
                if closed.contains(&next_node) {
                    continue;
                }

                let t_g_score =
                    self.get_g_score(&node) + self.map[next_node.coords.0][next_node.coords.1];
                let mut is_better = false;

                if !open.contains(&next_node) {
                    open.insert(next_node.clone());

                    self.set_h_score(&next_node, self.calc_h(&next_node));
                    is_better = true;
                } else if t_g_score < self.get_g_score(&next_node) {
                    is_better = true;
                }

                if is_better {
                    self.set_g_score(&next_node, t_g_score);
                    self.set_f_score(&next_node, t_g_score + self.get_h_score(&next_node))
                }
            }
        }
    }

    fn calc_h(&self, state: &State) -> usize {
        // if self.rows - 1 == state.coords.0
        //     && self.cols - 1 == state.coords.1
        //     && state.forward_counter < 3
        // {
        //     return usize::MAX / 2;
        // }

        // if state.coords.0 == self.rows - 1 {
        //     if state.direction == Direction::East {
        //         if self.cols - 1 - state.coords.1 + state.forward_counter < 3 {
        //             return usize::Max / 2;
        //         } else {
        //             // sum of
        //         }
        //     } else {
        //
        //     }
        // }

        let dir_h = match state.direction {
            Direction::North => 3,
            Direction::East => 1,
            Direction::South => 0,
            Direction::West => 2,
        };

        self.rows - state.coords.0 + self.cols - state.coords.1 + dir_h
    }

    fn set_h_score(&mut self, state: &State, score: usize) {
        self.h_score[state.coords.0][state.coords.1][state.direction.index()]
            [state.forward_counter] = score;
    }

    fn set_g_score(&mut self, state: &State, score: usize) {
        self.heat_loss[state.coords.0][state.coords.1][state.direction.index()]
            [state.forward_counter] = score;
    }

    fn set_f_score(&mut self, state: &State, score: usize) {
        self.f_score[state.coords.0][state.coords.1][state.direction.index()]
            [state.forward_counter] = score;
    }

    fn get_h_score(&self, state: &State) -> usize {
        self.h_score[state.coords.0][state.coords.1][state.direction.index()][state.forward_counter]
    }

    fn get_g_score(&self, state: &State) -> usize {
        self.heat_loss[state.coords.0][state.coords.1][state.direction.index()]
            [state.forward_counter]
    }

    fn get_f_score(&self, state: &State) -> usize {
        self.f_score[state.coords.0][state.coords.1][state.direction.index()][state.forward_counter]
    }

    fn score(&self) -> Option<usize> {
        println!("{:?}", self.heat_loss[self.rows - 1][self.cols - 1]);

        self.heat_loss[self.rows - 1][self.cols - 1]
            .iter()
            .map(|v| {
                v.iter()
                    .enumerate()
                    .filter(|(i, h)| *i > 2)
                    .map(|(i, h)| *h)
                    .collect::<Vec<_>>()
            })
            .flatten()
            .min()
    }

    pub fn parse(lines: Vec<String>) -> anyhow::Result<Self> {
        if lines.is_empty() {
            bail!("Empty input data")
        }

        let rows = lines.len();
        let cols = lines[0].len();

        let mut heat_loss = vec![vec![vec![vec![usize::MAX; 10]; 4]; cols]; rows];
        let f_score = vec![vec![vec![vec![usize::MAX; 10]; 4]; cols]; rows];
        let h_score = vec![vec![vec![vec![usize::MAX; 10]; 4]; cols]; rows];

        heat_loss[0][0][Direction::East.index()][0] = 0;
        // heat_loss[0][0][Direction::South.index()][0] = 0;

        let mut map = vec![Vec::new(); rows];

        for (row_index, line) in lines.into_iter().enumerate() {
            for symbol in line.chars() {
                let field = symbol
                    .to_digit(10)
                    .context("Failed to parse field heat loss")?;

                map[row_index].push(field as usize);
            }
        }

        Ok(Self {
            rows,
            cols,
            map,
            heat_loss,
            f_score,
            h_score,
        })
    }
}

impl Display for HeatMap {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for row in 0..self.rows {
            for col in 0..self.cols {
                let min = *self.heat_loss[row][col].iter().flatten().min().unwrap();
                let field = self.map[row][col];
                write!(f, "{:4} ({})", min, field)?;
            }

            write!(f, "\n")?;
        }

        Ok(())
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let mut heat_map = HeatMap::parse(lines)?;

    heat_map.a_star(Part::First);

    println!("{:?}", heat_map.score());

    Ok(())
}
