use anyhow::Result;
use aoc2023::d10::Area;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let area = Area::parse(lines)?;

    let distance = area.get_pipes_loop_max_distance();

    println!("{:?}", distance);

    Ok(())
}
