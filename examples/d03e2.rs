use anyhow::Result;
use aoc2023::d03::EngineSchematic;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let engine_schematic = EngineSchematic::new(lines)?;

    let sum = engine_schematic.gear_ratios().sum::<u32>();

    println!("{sum}");

    Ok(())
}
