use anyhow::Result;
use aoc2023::d19::System;
use aoc2023::read_input_data;

#[tokio::main]
async fn main() -> Result<()> {
    let input_data = read_input_data().await?;

    let system = System::parse(input_data)?;

    println!("{:?}", system.score2());

    Ok(())
}
