use anyhow::Result;
use aoc2023::d04::Lottery;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let lottery: Lottery = lines.try_into()?;

    println!("{}", lottery.score2());

    Ok(())
}
