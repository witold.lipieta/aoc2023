use anyhow::Result;
use aoc2023::d23::{Graph, Map};
use aoc2023::input_as_lines;
use std::fmt::Display;

fn part1(map: &Map) -> i64 {
    let start = map.starting_node();
    let goal = map.ending_node();

    let graph = Graph::read_map(map);

    let nodes = graph.kahn(start);

    let distances = graph.calculate_distances(nodes, start, goal);

    let longest_path_distance = distances.get(&goal).unwrap() - 1;

    longest_path_distance
}

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let map = Map::parse(lines)?;

    let longest_path_distance = part1(&map);

    println!("{}", longest_path_distance);

    Ok(())
}
