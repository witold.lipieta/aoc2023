use anyhow::{Context, Result};
use aoc2023::d01::TwoDigitNumber;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let numbers = lines
        .iter()
        .map(|line| TwoDigitNumber::parse(line.as_str()))
        .collect::<Result<Vec<_>>>()
        .context("Failed to parse input data")?;

    let sum = numbers
        .iter()
        .map(|number| Into::<u32>::into(number))
        .sum::<u32>();

    println!("{}", sum);

    Ok(())
}
