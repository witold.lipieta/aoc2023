use anyhow::Result;
use aoc2023::d11::Universe;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let universe = Universe::parse(lines, 999999)?;

    println!("{:?}", universe.sum_of_distances());

    Ok(())
}
