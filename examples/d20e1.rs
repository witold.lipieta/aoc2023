use anyhow::{bail, Context, Result};
use aoc2023::input_as_lines;
use std::collections::{HashMap, VecDeque};

#[derive(Debug, PartialEq, Clone)]
enum ModuleType {
    Broadcast,
    FlipFlop(bool),
    Conjunction(HashMap<String, bool>),
    Dummy(bool),
}

#[derive(Debug)]
struct Module {
    module_type: ModuleType,
    destinations: Vec<String>,
}

impl Module {
    fn pulse(&mut self, from: String, is_high: bool) -> Vec<(String, bool)> {
        let module_type = self.module_type.clone();
        match module_type {
            ModuleType::Broadcast => self
                .destinations
                .iter()
                .map(|d| (d.clone(), false))
                .collect(),
            ModuleType::FlipFlop(state) => {
                if !is_high {
                    self.module_type = ModuleType::FlipFlop(!state);

                    self.destinations
                        .iter()
                        .map(|d| (d.clone(), !state))
                        .collect()
                } else {
                    Vec::new()
                }
            }
            ModuleType::Conjunction(mut inputs) => {
                inputs.insert(from, is_high);

                let output =
                    inputs.values().filter(|pulse| **pulse).count() != inputs.iter().count();

                self.module_type = ModuleType::Conjunction(inputs);

                self.destinations
                    .iter()
                    .map(|d| (d.clone(), output))
                    .collect()
            }
            ModuleType::Dummy(_) => {
                if !is_high {
                    self.module_type = ModuleType::Dummy(true);
                }
                Vec::new()
            }
        }
    }

    fn dummy() -> Self {
        Self {
            module_type: ModuleType::Dummy(false),
            destinations: Vec::new(),
        }
    }

    fn parse(input: String) -> Result<(Self, String)> {
        let (module, destinations) = input.split_once(" -> ").context("Wrong input for module")?;

        let destinations = destinations.split(", ").map(|d| d.to_string()).collect();

        let (module_type, name) = if module.starts_with("broadcast") {
            (ModuleType::Broadcast, "broadcast".to_string())
        } else if let Some(name) = module.strip_prefix("%") {
            (ModuleType::FlipFlop(false), name.to_string())
        } else if let Some(name) = module.strip_prefix("&") {
            (ModuleType::Conjunction(HashMap::new()), name.to_string())
        } else {
            bail!("");
        };

        Ok((
            Self {
                module_type,
                destinations,
            },
            name,
        ))
    }
}

#[derive(Debug)]
struct ModuleConfiguration {
    modules: HashMap<String, Module>,
}

impl ModuleConfiguration {
    fn score2(&mut self) -> usize {
        let mut count = 0;

        for _ in 0..5 {
            count += 1;
            if count % 1000usize == 0 {
                print!("\r");
                print!("{count}");
            }
            println!("{:?}", self.push_button());
            if let Some(rx) = self.modules.get("rx") {
                if rx.module_type == ModuleType::Dummy(true) {
                    break;
                }
            }
        }

        count
    }

    fn score(&mut self) -> usize {
        let mut low_pulses = 0;
        let mut high_pulses = 0;

        for _ in 0..1000 {
            let (h, l) = self.push_button();
            low_pulses += l;
            high_pulses += h;
        }

        low_pulses * high_pulses
    }

    fn push_button(&mut self) -> (usize, usize) {
        let mut low_pulses = 0;
        let mut high_pulses = 0;
        let mut pulses = VecDeque::new();

        pulses.push_back(("button".to_string(), "broadcast".to_string(), false));

        while let Some((from, destination, is_high)) = pulses.pop_front() {
            // println!("F: {}, D: {}, P: {}", from, destination, is_high);
            if is_high {
                high_pulses += 1;
            } else {
                low_pulses += 1;
            }
            let next_pulses = self
                .modules
                .get_mut(&destination)
                .unwrap()
                .pulse(from, is_high);
            for (next_pulse, next_is_high) in next_pulses {
                pulses.push_back((destination.clone(), next_pulse, next_is_high));
            }
        }

        (high_pulses, low_pulses)
    }

    fn parse(input: Vec<String>) -> Result<Self> {
        let mut modules = HashMap::new();

        for line in input {
            let (module, name) = Module::parse(line)?;
            modules.insert(name, module);
        }

        let mut inputs: HashMap<String, Vec<String>> = HashMap::new();

        for (input_name, input) in &modules {
            for destination in &input.destinations {
                inputs
                    .entry(destination.clone())
                    .and_modify(|d| d.push(input_name.clone()))
                    .or_insert(vec![input_name.clone(); 1]);
            }
        }

        for (input, _) in &inputs {
            modules.entry(input.clone()).or_insert(Module::dummy());
        }

        for (name, module) in modules.iter_mut() {
            module.module_type = match &module.module_type {
                ModuleType::Conjunction(_) => {
                    let mut conjunction_inputs = HashMap::new();

                    for input in inputs.get(name).unwrap() {
                        conjunction_inputs.insert(input.clone(), false);
                    }

                    ModuleType::Conjunction(conjunction_inputs)
                }
                mt => mt.clone(),
            }
        }

        Ok(Self { modules })
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let mut configuration = ModuleConfiguration::parse(lines)?;

    // println!("{:?}", configuration);
    println!("{:?}", configuration.score());

    Ok(())
}
