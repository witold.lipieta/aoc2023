use anyhow::Result;
use aoc2023::d08::Map;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let map = Map::parse(lines)?;

    println!("{:?}", map.count_steps2());

    Ok(())
}
