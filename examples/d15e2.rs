use anyhow::Result;
use aoc2023::d15::InitializationSequence;
use aoc2023::read_input_data;

#[tokio::main]
async fn main() -> Result<()> {
    let input_data = read_input_data().await?;

    let mut sequence = InitializationSequence::parse2(input_data)?;

    println!("{}", sequence.score2());

    Ok(())
}
