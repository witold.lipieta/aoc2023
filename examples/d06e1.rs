use anyhow::Result;
use aoc2023::d06::Races;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let races = Races::parse(lines)?;

    println!("{:?}", races.score());

    Ok(())
}
