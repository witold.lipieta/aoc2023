use anyhow::Result;
use aoc2023::d02::Game;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let games = lines
        .iter()
        .map(|line| line.as_str().try_into())
        .collect::<Result<Vec<Game>>>()?;

    let sum = games.iter().map(|game| game.score()).sum::<i32>();

    println!("{sum}");

    Ok(())
}
