use anyhow::Result;
use aoc2023::d12::SpringRow;
use aoc2023::input_as_lines;

#[tokio::main]
async fn main() -> Result<()> {
    let lines = input_as_lines().await?;

    let sum = lines
        .iter()
        .map(|line| SpringRow::parse2(line))
        .collect::<Result<Vec<_>>>()?
        .iter()
        .map(|row| row.arrangements())
        .sum::<usize>();

    println!("{:?}", sum);

    Ok(())
}
